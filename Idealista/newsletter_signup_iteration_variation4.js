/*
Domain: Idealista.fi
Experiment name: Idealista newsletter sign up pop up variation 4
Version: 1.0
Platform: VWO
Author: Thomas
*/

//Newsletter content variables
var newsletterBox = document.querySelector('#newsletterBox');
var newsletterBoxClose = document.querySelector('#newsletterBoxClose span');
var newsLetterHeadline = document.querySelector('#newsletterBox h3');
var newsLetterParagraph = document.querySelector('#newsletterBox p');
var newsletterCTA = document.querySelector('#newsletterBoxCTA');
var newsletterCTALink = document.querySelector('#newsletterBoxCTA a').href;
var newsletterCTALinkTarget = document.querySelector('#newsletterBoxCTA a').target;

//Edit contents
newsLetterHeadline.innerText = "Tilaa Idealistan uutiskirje";
newsLetterParagraph.innerText = "ja saat kiinnostavimmat jutut suoraan sähköpostiisi!";

//Styles
//Background
newsletterBox.style = "background: url('https://s22.postimg.cc/4tbhhdg5d/image.jpg') no-repeat; background-size: cover; text-align: center;";
newsletterBoxClose.style = "color: #fff; background: #000; padding: .5% 1%;";
newsLetterHeadline.style = "font-weight: bold; font-size: 3em; font-style: normal;";
newsLetterParagraph.style = "font-family: 'Montserrat', sans-serif !important; font-weight: bold; font-size: 1.6em;";
newsletterCTA.children[0].style = "padding: 15px; font-size: 18px;";

//Mobile styles
if ( window.innerWidth < 768 ) {
  newsletterBox.style.top = "30px";
  newsletterCTAButton.style.fontSize = "1.33em";
  newsletterCTAButton.style.padding = "5% 1%";
}
