/*
Domain: Idealista.fi
Experiment name: Idealista newsletter sign up pop up variation 2
Version: 1.0
Platform: VWO
Author: Thomas
*/

//Newsletter content variables
var newsletterBox = document.querySelector('#newsletterBox');
var newsletterBoxClose = document.querySelector('#newsletterBoxClose span');
var newsLetterHeadline = document.querySelector('#newsletterBox h3');
var newsLetterParagraph = document.querySelector('#newsletterBox p');
var newsletterCTA = document.querySelector('#newsletterBoxCTA');
var newsletterCTALink = document.querySelector('#newsletterBoxCTA a').href;
var newsletterCTALinkTarget = document.querySelector('#newsletterBoxCTA a').target;

//Edit contents
newsletterCTA.innerHTML = "<a id='surveyLink' href='"+newsletterCTALink+"' target='"+newsletterCTALinkTarget+"'>Tilaa uutiskirje &#187;</a>";
//New CTA button variable
var newsletterCTAButton = document.querySelector('a#surveyLink');

newsLetterHeadline.innerText = "Tilaa Idealistan uutiskirje";
newsLetterParagraph.innerText = "ja saat kiinnostavimmat jutut suoraan sähköpostiisi!";

//Styles
newsletterBox.style = "background: url('https://s22.postimg.cc/4tbhhdg5d/image.jpg') no-repeat; background-size: cover; text-align: center;";
newsletterBoxClose.style = "color: #fff; background: #000; padding: .5% 1%;";
newsLetterHeadline.style = "font-weight: bold; font-size: 3em; font-style: normal;";
newsLetterParagraph.style = "font-family: 'Montserrat', sans-serif !important; font-weight: bold; font-size: 1.6em;";
newsletterCTA.children[0].style = "background: none; max-width: 400px; padding: 0;";
newsletterCTAButton.style = "background: rgba(0,0,0,.75); border-radius: 50px; -webkit-box-shadow: 0px 0px 20px 2.5px rgba(255,181,201,1); -moz-box-shadow: 0px 0px 20px 2.5px rgba(255,181,201,1); 0px 0px 20px 2.5px rgba(255,181,201,1); max-width: 400px; padding: 2%; display:block; text-align: center; font-size: 1.7em; -webkit-text-stroke-width: 1px; -webkit-text-stroke-color: #ff8fad; color: rgba(0,0,0,.0); border: 2px solid #ff8fad; letter-spacing: 1.5px; margin: auto;";

//Mobile styles
if ( window.innerWidth < 768 ) {
  newsletterBox.style.top = "30px";
  newsletterCTAButton.style.fontSize = "1.33em";
  newsletterCTAButton.style.padding = "5% 1%";
}
