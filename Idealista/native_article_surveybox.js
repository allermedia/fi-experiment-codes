/*
Domain: Idealista.fi
Experiment name: Idealista native article survey pop-up experiment
Version: 1.0
Platform: Optimizely
Author: Thomas
*/

//Append survey pop-up box to page
$('body').append('<div id="surveyBox"><div id="surveyBoxClose"><span>&#10005;</span></div><b id="surveyBoxHeadline">Haluaisimme kuulla mielipiteesi jutusta</b><h3>"Pauli Aalto-Setälä: Itsensä johtamisen kolme estettä"</h3><div id="surveyBoxCTA"><a>Vastaa kyselyyn (2-3 min.)</a></div></div>');

//Hide survey box if seen previously
if ( document.cookie.indexOf('hasSeenSurveyBox') !== -1 ) {
  document.getElementById("surveyBox").style.display = "none";
}

//Show box after 5 secs
setTimeout(function(){
    //Define survey box element
    var surveyBox = document.getElementById("surveyBox");
    //Show survey boxes
    surveyBox.style.visibility = "visible";
    //Add animation class
    surveyBox.className += "animateIn";
    //Hide box on click
    document.getElementById("surveyBoxClose").addEventListener("click", function(){
        document.getElementById("surveyBox").style.display = "none";
        //Define date variable
        var d = new Date();
        d.setTime(d.getTime() + (30*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        //Set cookie
        document.cookie = "hasSeenSurveyBox=1;" + expires + ";";
    });
    //Event listener for CTA button
    document.getElementById("surveyBoxCTA").addEventListener("click", function(){
        document.getElementById('#surveyBoxHeadline').innerHTML = 'Kiitos mielenkiinnosta. Valitettavasti kysely on suljettu.';
        document.getElementById('#surveyBoxCTA').style.display = "none";
        document.querySelector('#surveyBox h3').style.display = "none";
        //Define date variable
        var d = new Date();
        d.setTime(d.getTime() + (30*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        //Set cookie
        document.cookie = "hasSeenSurveyBox=1;" + expires + ";";
    });
}, 5000);
