/*
Domain: Keskustelu.suomi24.fi
Experiment name: Vote feature optimization
Version: 1.2
Platform: VWO
Author: Pan
*/

// Variant 1: to the right
$(".action-bar .action-bar-voting").removeClass("pull-left").addClass("pull-right");
// move the vote number to between the vote button
$(".thread .action-bar-voting").each(function(){   
    let votenumber = $(this).find(".action-bar-vote-count")[0];
    $(votenumber).detach().insertAfter($(this).find(".action-bar-like"));    
});

// Variant 2: to the right - icon only
$(".action-bar .action-bar-share").html('').append('<i class="fa fa-share fa-lg"></i>');
$(".action-bar .action-bar-report").html('').append('<i class="fa fa-flag fa-lg"></i>');
$(".action-bar .action-bar-voting").removeClass("pull-left").addClass("pull-right");
// move the vote number to between the vote button
$(".thread .action-bar-voting").each(function(){   
    let votenumber = $(this).find(".action-bar-vote-count")[0];
    $(votenumber).detach().insertAfter($(this).find(".action-bar-like"));    
});




// Variant 3: to the right at side
$(".action-bar .action-bar-share").html('').append('<i class="fa fa-share fa-lg"></i>');
$(".action-bar .action-bar-report").html('').append('<i class="fa fa-flag fa-lg"></i>');
$(".action-bar-like > i").removeClass("fa-thumbs-up").addClass("fa-angle-up").css("font-size",'40px');
$(".action-bar-dislike > i").removeClass("fa-thumbs-down").addClass("fa-angle-down").css("font-size",'40px');


$(".action-bar .action-bar-voting").removeClass("pull-left").addClass("pull-right");
// move the vote number to between the vote button
$(".action-bar-voting").each(function(){   
    let votenumber = $(this).find(".action-bar-vote-count")[0];
    $(votenumber).css("margin-left","14px").detach().insertAfter($(this).find(".action-bar-like"));
});

// move the vote container and set its width
$(".answer-body-container").css({
    "width": "85%",
    "float": "left"
});
$(".answer-block-container:not(.answer-removed) .answer").each(function(){
   $(this).find(".action-bar-voting").css({
    "width": "15%",
    "position": "absolute",
    "bottom": "42px",
    "right": 0
    }).detach().insertAfter($(this).find(".answer-body-container")); 
    $(".answer-body-container").css("min-height", "88px");
});
// set comment container width
$(".comments-list .comment").each(function() {
    $(this).find(".action-bar-voting").css({
        "width": "15%",
        "position": "absolute",
        "bottom": "42px",
        "right": 0
    }).detach().insertAfter($(this).find(".comment-container"));
});
$(".comment-container").css({
    "width": "85%",
    "float": "left"
});
// make the action bar one line
$(".action-bar").css({"clear":"both"});



// Variant 4: to the right at sidd - icon only 