/*
Domain: Suomi24.fi
Experiment name: Telvis banner in right column
Version: 1.0
Platform: VWO
Author: Thomas
*/

//Banner content
$('<div id="telvisBanner"></div>').prependTo('.column3');
$('#telvisBanner').append('<img id="telvisBanner--image" src="https://www.telvis.fi/static/img/telvis-logo.png">');
$('#telvisBanner').append('<b>Tämänkin ohjelman esitysajat löydät Telviksestä</b><a href="https://telvis.fi">Katso ohjelmatiedot</a>');

//Styles
$('#telvisBanner').css({
  'background' : '#000',
  'color' : '#fff',
  'padding' : '5% 7.5%',
  'margin' : '5% 0'
});

$('#telvisBanner--image').css({
'margin' : '5% auto 5%',
'display' : 'block',
'width' : '100px'
});

$('#telvisBanner a').css({
  'background' : '#EC1D24',
  'display' : 'block',
  'border-radius' : '30px',
  'padding' : '2.5% 5%',
  'text-align' : 'center',
  'color' : '#fff',
  'margin' : '5% auto'
});

$('#telvisBanner b').css({
  'text-align' : 'center',
  'width' : '100%',
  'display' : 'block',
  'font-size' : '1.25em',
  'font-weight' : '400'
});

//Desktop styles
if ( window.innerWidth > 768 ) {
  $('#telvisBanner').css({
    'padding' : '2.5% 5% 2% 5%'
  });
}
