/*
Domain: Suomi24.fi
Experiment name: Buy visibility product to your question thread experiment (POC, never published)
Version: 1.1
Platform: -
Author: Thomas
*/

//Define elements
var questionRadioContainer = document.querySelectorAll('.thread-type-radio-question')[0];
var questionRadio = document.getElementsByClassName('radio-inline')[0].querySelector('span');
var infoIconRadio = document.querySelectorAll('[data-container="div.thread-type-radio-question"]')[0];
//Variable to do thing once
var isChecked = 0;
//Event listener (click event)
questionRadio.addEventListener('click', function(){
  isChecked++;
  //Check if button is clicked once
  if ( isChecked === 1 ) {
    //Create div and content
    var upsellContainer = document.createElement("div");
    upsellContainer.id = "bump_upsell";
    upsellContainer.innerHTML = "<h2 id='headlineH2'>Haluatko lisänäkyvyyttä kysymykseesi?</h2><p>Pienellä lisämaksulla voit saada jopa <b>10x </b>lisää näkyvyyttä!</p><a id='showmoreCTA'>Lue lisää</button></a>";
    //Append content box to main container
    questionRadioContainer.appendChild(upsellContainer);
    //Styles
    document.getElementById("bump_upsell").style.padding = "0px 2% 2% 3%";
    document.getElementById("bump_upsell").style.border = "1px solid #CCC";
    document.getElementById("bump_upsell").style.backgroundColor = "#FFF";
    document.getElementById("headlineH2").style.fontSize = "1.25em";
    infoIconRadio.style.top = "30px";
  }
});
