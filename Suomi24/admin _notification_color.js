/*
Domain: suomi24.fi
Experiment name: admin notification color
Version: 1.0
Platform: VWO
Author: Pan
*/

// change UI
$("<div id='adminnotification2' class='adminnotifications js-cmp navbar navbar-secondary' data-component='admin-notification-to-user'><div class='row'><div class='pull-right'><button type='button' class='btn btn-link admin-notifications-button' data-max-start-date='2018-10-09 10:22:57'><span class='fa fa-times'></span></button></div><div class='col-md-12 admin-notification-col'><div class='col-md-12 notification-info'><span class='headingContainer'><span class='heading pull-left'>NYT PINNALLA: </span></span><span class='descriptionContainer'><span class='description pull-left'><a href='https://keskustelu.suomi24.fi/suomi24-blogi-' target='_blank'>Keskustele Suomi24 Keskustelujen kehityksestä tuoreessa blogissa. </a></span></span></div></div></div><div class='adminnotification-template hidden'><div class='col-md-12 notification-info'><span class='headingContainer'><span class='heading pull-left'></span></span><span class='descriptionContainer'><span class='description pull-left'></span></span></div></div></div>").insertAfter(".adminnotifications");
$("body").on('click', "#adminnotification2 button", function(){
  $("#adminnotification2").addClass("hidden");
});



// CSS dark green
.adminnotifications {
    background-color:#88b400; 
    padding-right: 13px; 
    margin: 0;
    border: none;
    padding: 0 13px 0 0;
  }
  .admin-notifications-button {
    margin-bottom: 0;
    color: white;
    font-size: large;
    padding: 10px;
  }
  .admin-notification-col {
    width: -webkit-calc(100% - 37px);
    width:    -moz-calc(100% - 37px);
    width:         calc(100% - 37px);
  }
  .adminnotifications .heading      { margin: 13px 10px 0 0; color: white;}
  .adminnotifications .description  { padding: 13px 0 10px 0; color: white; }
  .adminnotifications .description a:not(:hover) { color: white; }
  .www-no-premium-adminnotification { margin: 0px -15px 0px -15px; }


// light green
  .adminnotifications {
    background-color:#88ef35;
    padding-right: 13px; 
    margin: 0;
    border: none;
    padding: 0 13px 0 0;
  }
  .admin-notifications-button {
    margin-bottom: 0;
    color: black;
    font-size: large;
    padding: 10px;
  }
  .admin-notification-col {
    width: -webkit-calc(100% - 37px);
    width:    -moz-calc(100% - 37px);
    width:         calc(100% - 37px);
  }
  .adminnotifications .heading      { margin: 13px 10px 0 0; color: black;}
  .adminnotifications .description  { padding: 13px 0 10px 0; color: black; }
  .adminnotifications .description a:not(:hover) { color: black; }
  .www-no-premium-adminnotification { margin: 0px -15px 0px -15px; }

  // dark yellow
  .adminnotifications {
    background-color:#ffa90e;
    padding-right: 13px; 
    margin: 0;
    border: none;
    padding: 0 13px 0 0;
  }
  .admin-notifications-button {
    margin-bottom: 0;
    color: white;
    font-size: large;
    padding: 10px;
  }
  .admin-notification-col {
    width: -webkit-calc(100% - 37px);
    width:    -moz-calc(100% - 37px);
    width:         calc(100% - 37px);
  }
  .adminnotifications .heading      { margin: 13px 10px 0 0; color: white;}
  .adminnotifications .description  { padding: 13px 0 10px 0; color: white; }
  .adminnotifications .description a:not(:hover) { color: white; }
  .www-no-premium-adminnotification { margin: 0px -15px 0px -15px; }

  // light yellow
  .adminnotifications {
    background-color:#FFEC08;
    padding-right: 13px; 
    margin: 0;
    border: none;
    padding: 0 13px 0 0;
  }
  .admin-notifications-button {
    margin-bottom: 0;
    color: black;
    font-size: large;
    padding: 10px;
  }
  .admin-notification-col {
    width: -webkit-calc(100% - 37px);
    width:    -moz-calc(100% - 37px);
    width:         calc(100% - 37px);
  }
  .adminnotifications .heading      { margin: 13px 10px 0 0; color: black;}
  .adminnotifications .description  { padding: 13px 0 10px 0; color: black; }
  .adminnotifications .description a:not(:hover) { color: black; }
  .www-no-premium-adminnotification { margin: 0px -15px 0px -15px; }