/*
Domain: Suomi24.fi
Experiment name: Suomi24 application promotion experiment
Version: 2.0
Platform: VWO
Author: Thomas
*/

//Application promotion banner content
var bannerContainerStart = "<div id='applicationBanner'>";
var bannerIcon = "<img id='bannerIcon' src='https://www.suomi24.fi/img/logos/admin_highlight.svg'>";
var bannerCTA = "<div id='bannerCTA' class='btn btn-primary btn-sm'>Lataa</div>";
var bannerClose = "<span id='bannerClose'>&#10005;</span>";
var bannerStars = '<div id="bannerStars"><img src="https://s9.postimg.cc/iun023027/star.png"><img src="https://s9.postimg.cc/iun023027/star.png"><img src="https://s9.postimg.cc/iun023027/star.png"><img src="https://s9.postimg.cc/iun023027/star.png"><span id="bannerReviewCount">(1540+)</span></div>';
var bannerText = "<div id='bannerParagraph'>Keskustele helposti mistä vain</div>";
var bannerHeadline = "<b id='bannerHeadline'>Suomi24 Keskustelu</b>";
var bannerContainerEnd= "</div>";
var applicationBanner = bannerContainerStart+bannerCTA+bannerClose+bannerIcon+bannerHeadline+bannerText+bannerStars+bannerContainerEnd;

//Main page container
var mainContainer = document.getElementsByClassName('main-container')[0];

//Cookie policy box check
var cookiePolicyBox = document.getElementsByClassName('s24_cc_banner-wrapper').length === 0;
if ( cookiePolicyBox ) {
    $('body').prepend(applicationBanner);
}

//Styles
var bannerContainerStyles = document.getElementById('applicationBanner');
var bannerIconStyles = document.getElementById('bannerIcon');
var bannerHeadlineStyles = document.getElementById('bannerHeadline');
var bannerTextStyles = document.getElementById('bannerParagraph');
var bannerCTAStyles = document.getElementById('bannerCTA');
var bannerStarContainerStyles = document.getElementById('bannerStars');
var bannerStarsStyles = document.querySelectorAll('#bannerStars img');
var bannerCloseStyles = document.getElementById('bannerClose');
var bannerReviewCountStyles = document.getElementById('bannerReviewCount');

bannerContainerStyles.style = "background: #fff; padding: 4% 2%; position: fixed; bottom: 0; z-index: 10; width: 100%; border: 1px solid #c0c0c0;";
bannerIconStyles.style = "width: 45px; background: white; float:left; padding: 5px; margin: 0 10px 0 0; border: 1px solid #cbcbcb; border-radius: 10px;";
bannerHeadlineStyles.style = "font-size: 1.1em; margin: 10px 0;";
bannerTextStyles.style = "font-size: .9em; color: #acacac;";
bannerCloseStyles.style = "display: block; float:left; font-size: 1.2em; padding: 10px 10px 10px 0;";
bannerStarContainerStyles.style = "color: #FEAF50; margin: 0 0 10px; float: left;";
bannerCTAStyles.style = "background: #76c932; box-shadow: 0 6px 0 #57a01c; border: 1px solid #76c932; margin: 0 10px 0 0; float: right; text-transform: uppercase;";
bannerReviewCountStyles.style = "font-size: .6em; color: #c0c0c0; margin: 0 0 0 5px;";

for ( var i=0; i < 4; i++ ) {
  bannerStarsStyles[i].style = "margin: 0 2px 0 0; width: 10px;";
}

//Functionality on close button click
bannerCloseStyles.addEventListener("click", function(){
    bannerContainerStyles.style = "display: none;";
    //Set cookie valid for a month
    var d = new Date();
    d.setTime(d.getTime() + (30*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = "s24App=1;"+ expires + ";path=/";
});

//Functionality on CTA button click
bannerCTAStyles.addEventListener("click", function(){
    bannerCTAStyles.style = "display: none;";
    bannerIconStyles.style = "display: none;";
    bannerCTAStyles.style = "display: none;";
    bannerTextStyles.style = "display: none;";
    bannerStarContainerStyles.style = "display: none;";
    bannerHeadlineStyles.outerHTML = "<p id='bannerHeadline'><b>Mahtavaa, että kiinnostuit Suomi24-sovelluksesta!</b><br/> Sovellus on vielä työn alla, mutta kerro meille, millainen sen pitäisi olla.<br/><div id='bannerCTA' class='btn btn-primary btn-sm surveyLink'>Vastaa kyselyyn (1 min.)</div></p>";
    $('#bannerCTA').css("margin","0 0 0 25px");
    //Set cookie valid for a month
    var d = new Date();
    d.setTime(d.getTime() + (30*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = "s24App=1;"+ expires + ";path=/";

    //Go to survey
    var surveyLink = document.getElementsByClassName('surveyLink')[0];
    surveyLink.addEventListener("click", function(){
      window.location.href = "https://insights.hotjar.com/s?siteId=718377&surveyId=57843";
    });

});

//Mobile styles
if ( window.innerWidth <= 325 ) {
  $('#bannerCTA').insertAfter('#bannerStars');
  bannerCTAStyles.style.margin = "5px 0 0 0";
  bannerCTAStyles.style.display = "block";
  bannerCTAStyles.style.float = "none";
  bannerStarContainerStyles.style = "float: none;";
}
