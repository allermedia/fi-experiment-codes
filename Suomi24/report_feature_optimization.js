/*
Domain: Suomi24.fi
Experiment name: Report feature optimization
Version: 1.0
Platform: VWO
Author: Pan
*/

// get all the report buttons
var btn_report = $(".action-bar-report");

// Change the text and colo, 
for(var i = 0; i < btn_report.length; i++){
    btn_report[i].innerHTML = "Asiaton sisältö";
    btn_report[i].style.color = "#d00000";
}

$(".thread").on("click", ".action-bar-report", function(){


    // hide the text input and remove the line break
    $(".action-bar-dropdown.report.open .action-bar-dropdown-container .form-group.has-feedback:not(.captcha-img-container").addClass("hidden").css("margin:0;");
    $(".action-bar-dropdown.report.open .action-bar-dropdown-container br").remove();

    // Add a hint text to the top of the form
    var hint_text = "<div class='report-form-hint-text'style='margin:15px 15px 0 15px;font-weight: bold;'> Valitse yksi vaihtoehdoista <span class='text-secondary' >(pakollinen)</span> </div>";
    $(".action-bar-dropdown.report.open").prepend(hint_text);

    // remove the margin-top
    $(".action-bar-dropdown.report.open .action-bar-dropdown-container").css("padding-top","0");
});