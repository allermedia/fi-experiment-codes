/*
Domain: Suomi24.fi
Experiment name: Telvis movies embed on Tv section header
Version: 1.0
Platform: VWO
Author: Thomas
*/

//Widget container
$('.topic-description a').html('<a href="https://telvis.fi"><div id="leffat_tanaan"></div></a>');

//Header content
$('#leffat_tanaan').append('<a id="leffat_tanaan__header" href="https://telvis.fi">Leffat tänään</a>');

//Remove extra spacing
$('.topic-description br').remove();

//YLE
//$('#leffat_tanaan').append('<img class="channel" src="https://s17.postimg.org/m8oa7ggrz/yle_tv1.png"><div class="tvShow"><b>	Underworld: Rise of the Lycans -</b><span> 21:00</span></div><br/>');
//$('#leffat_tanaan').append('<img class="channel" src="https://s17.postimg.org/45v7g8sn3/yle_tv2.png"><div class="tvShow"><b>	Wrecked -</b><span> 21:00</span></div><br/>');
//MTV3
//$('#leffat_tanaan').append('<img class="channel" src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Mtv3_new_logo.svg/1200px-Mtv3_new_logo.svg.png"><div class="tvShow"><b>The Matador -</b><span> 21:00</span></div><br/>');
//Nelonen
//$('#leffat_tanaan').append('<img class="channel" src="https://s17.postimg.org/5tyy8sgin/nelonen.jpg"><div class="tvShow"><b>The Paperboy -</b><span> 21:00</span></div><br/>');
//Sub
$('#leffat_tanaan').append('<img class="channel" src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0e/Sub_new_logo.svg/1200px-Sub_new_logo.svg.png"><div class="tvShow"><b>The Matador -</b><span> 22:00</span></div><br/>');
//Jim
//$('#leffat_tanaan').append('<img class="channel" src="https://www.underconsideration.com/brandnew/archives/jim_tv_logo.png"><div class="tvShow"><b>	Wrecked -</b><span> 21:00</span></div><br/>');
//TV5
$('#leffat_tanaan').append('<img class="channel" src="http://discoverynetworks.fi/wp-content/uploads/2015/06/K5_logo_White.jpg"><div class="tvShow"><b>Gladiator -</b><span> 21:00</span></div><br/>');
//Kutonen
//$('#leffat_tanaan').append('<img class="channel" src="http://kutonen.fi/sites/default/files/KUTONEN_FINAL_LOGO.png"><div class="tvShow"><b>xXx 2: The Next Level -</b><span> 21:00</span></div><br/>');
//Frii
//Fox
//$('#leffat_tanaan').append('<img class="channel" src="http://madterrorfest.com/wp-content/uploads/2017/11/logo-tv-schedule-tv-guidenews-entertainment-and-celebrity-news-tv-news-and-free-logo-design-templates.jpg"><div class="tvShow"><b>Along Came a Spider -</b><span> 22:00</span></div><br/>');
//Hero
jQuery('#leffat_tanaan').append('<img class="channel" src="http://media.lily.fi/sites/lily/files/styles/wide/public/user/16486/2014/10/unnamed.png"><div class="tvShow"><b>Fracture -</b><span> 21:00</span></div><br/>');
$('#leffat_tanaan').append('<a class="cta" href="https://telvis.fi">Katso kaikki ohjelmatiedot</a>');

//Sponsored badge
$('#leffat_tanaan').append('<div class="sponsor"><span id="provided_by">Palvelun tuottaa</span><a href="https://telvis.fi"><img id="telvis" src="https://www.telvis.fi/static/img/telvis-logo.png"></a></div>');

//Styles
$('#leffat_tanaan').css({
  'margin' : '20px 0 10px -5px',
  'background' : '#000',
  'color' : '#fff',
  'padding' : '1% 1% 2% 0',
  'font-weight' : '400'
});

$('#leffat_tanaan__header').css({
  'position' : 'relative',
  'top' : '-20px',
  'background' : '#0076d0',
  'color' : '#fff',
  'padding' : '2%',
  'margin' : '2% 0px 0px',
  'display' : 'block',
  'text-transform' : 'uppercase',
  'width' : '101%'
});

$('#leffat_tanaan .tvShow').css({
  'display' : 'inline-block',
  'padding' : '1% 20px',
  'position' : 'relative',
  'top' : '3px'
});

$('#leffat_tanaan img.channel').css({
  'width' : '40px',
  'margin' : '0 5px 0 20px',
  'display' : 'inline'
});

$('#leffat_tanaan a.cta').css({
  'background' : '#EC1D24',
  'display' : 'block',
  'width' : '280px',
  'border-radius' : '30px',
  'padding' : '1.5% 5%',
  'text-align' : 'center',
  'color' : '#fff',
  'margin' : '3% 20px 1%'
});

$('.sponsor').css({
  'padding' : '7.5px 10px 0px 20px',
  'float' : 'right',
  'position' : 'relative',
  'top' : '-31px'
});

$('span#provided_by').css({
  'font-size' : '.75em',
  'margin' : '0 10px 0 0'
});

$('img#telvis').css({
  'width': '70px'
});

//Mobile styles
if ( window.innerWidth < 500 ) {
  $('.sponsor').css({
    'float' : 'initial',
    'top' : '0'
  });
}
