/*
Domain: Suomi24.fi
Experiment name: Telvis native banner in thread list
Version: 1.0
Platform: VWO
Author: Thomas
*/

//Clone thread item and add class
$('.thread-list-item:eq(0)').clone().insertAfter('.thread-list-item:eq(4)');
$('.thread-list-item:eq(5)').addClass('nativeBanner');

//Change content
$('.nativeBanner a').attr('href','https://www.telvis.fi/');
$('.nativeBanner .thread-list-item-timestamp').html('Mainos');
$('.nativeBanner .thread-list-item-comments').remove();
$('.nativeBanner .thread-list-item-likes').remove();
$('.nativeBanner .thread-list-item-title').text('Mitä telkkarista tulee tänään?');
$('.nativeBanner .thread-list-item-breadcrumb ').text('Telvis');
$('.nativeBanner .thread-list-item-body').text('Kaikkien ohjelmien tiedot löytyvät helposti Telviksestä!');
$('.nativeBanner .fa-stack').html('<i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-mobile fa-stack-1x fa-inverse"></i>');
