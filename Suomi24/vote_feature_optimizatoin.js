/*
Domain: Suomi24.fi
Experiment name: Vote feature optimization
Version: 1.0
Platform: VWO
Author: Pan
*/


// task change position
$(".action-bar .action-bar-reply").removeClass("pull-left").addClass("pull-right");
$(".action-bar .action-bar-voting").removeClass("pull-left").addClass("pull-right");
$(".action-bar .action-bar-report").remove();
$(".action-bar .action-bar-share").removeClass("pull-right").addClass("pull-left");
$('<div class="action-bar-report pull-left">Ilmianna</div>').insertAfter($(".action-bar .action-bar-share"));


// Use Arrow
$(".action-bar-like > i").removeClass("fa-thumbs-up").addClass("fa-angle-up").css("font-size",'40px');
$(".action-bar-dislike > i").removeClass("fa-thumbs-down").addClass("fa-angle-down").css("font-size",'40px');



// hover
    // .action-bar-button-container>div {
    //     line-height: 18px;
    // }
    // .action-bar-dislike:hover {
    //     background-color: red;
    //     color: white
    // }

    // .action-bar-like:hover {
    //     background-color: #06AD18;
    //     color: white
    // }
    
// vote up number  
$(".action-bar-vote-count").css({
    'background-color': '#06AD18',
    'color': 'white',
    'width': '30px'
})

// animation

{
<style>
@-moz-keyframes bounce {
  0%, 20%, 50%, 80%, 100% {
    -moz-transform: translateY(0);
    transform: translateY(0);
  }
  40% {
    -moz-transform: translateY(-12px);
    transform: translateY(-12px);
  }
  60% {
    -moz-transform: translateY(-6px);
    transform: translateY(-6px);
  }
}
@-webkit-keyframes bounce {
  0%, 20%, 50%, 80%, 100% {
    -webkit-transform: translateY(0);
    transform: translateY(0);
  }
  40% {
    -webkit-transform: translateY(-12px);
    transform: translateY(-12px);
  }
  60% {
    -webkit-transform: translateY(-6px);
    transform: translateY(-6px);
  }
}
@keyframes bounce {
  0%, 20%, 50%, 80%, 100% {
    -moz-transform: translateY(0);
    -ms-transform: translateY(0);
    -webkit-transform: translateY(0);
    transform: translateY(0);
  }
  40% {
    -moz-transform: translateY(-12px);
    -ms-transform: translateY(-12px);
    -webkit-transform: translateY(-12px);
    transform: translateY(-12px);
  }
  60% {
    -moz-transform: translateY(-6px);
    -ms-transform: translateY(-6px);
    -webkit-transform: translateY(-6px);
    transform: translateY(-6px);
  }
}
.bounce {
  -moz-animation: bounce 2s 2;
  -webkit-animation: bounce 2s 2;
  animation: bounce 2s 2;
  -webkit-animation-delay: 20s;
  animation-delay: 20s;
}
</style>
}


// Change thread view
$(".thread-list-item-likes").css({
    'background-color': '#06AD18',
    'color': 'white',
    'padding': '2px'
});
$(".thread-list-item-likes").closest(".thread-list-item").addClass("highlighted_voted_item").css({
    'background-color': '#e8f7e8'
}).find(".thread-list-item-comments").css({
    'background-color': '#06AD18',
    'color': 'white',
    'padding': '2px'
});




// big fat green thumbup
$(".thread .action-bar-voting").each(function(){
    
    let votenumber = $(this).find(".action-bar-vote-count")[0];

    $(votenumber).detach().prependTo($(this).find(".action-bar-like"));    
        
    if(votenumber.innerHTML !== "0"){
        $(votenumber).css({
            "margin-left": 0,
            "color": "white"
        });
        $(this).find(".action-bar-like").css({
            "background-color": "#06AD18",
            "color": "white",
            "padding": "0 12px"
        });
    }
});
// remove dislike button
$(".thread .action-bar-voting .action-bar-dislike").remove();
