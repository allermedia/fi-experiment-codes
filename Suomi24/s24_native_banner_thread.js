/*
Domain: Suomi24.fi
Experiment name: Suomi24 native thread banner experiment
Version: 1.0
Platform: Optimizely
Author: Thomas
*/

//Select Leiki container
var elementExists = document.querySelector(".column3 .leiki-container");

//If Leiki container exists
if ( document.body.contains(elementExists) == true) {
  //Headline 1
  var leikiBoxContentTitle = document.querySelector('[data-leiki-name="aller2"]').querySelectorAll('.recommended-thread-info .text-primary')[0].innerText;
  //Link 1
  var leikiBoxContentLink = document.querySelector('[data-leiki-name="aller2"]').querySelectorAll('a.recommended-thread-item')[0].href;
  //Source 1
  var leikiBoxContentSource = document.querySelector('[data-leiki-name="aller2"]').querySelectorAll('.recommended-thread-site')[0].innerText;

  //Headline 2
  var leikiBoxContentTitle2 = document.querySelector('[data-leiki-name="aller2"]').querySelectorAll('.recommended-thread-info .text-primary')[2].innerText;
  //Link 2
  var leikiBoxContentLink2 = document.querySelector('[data-leiki-name="aller2"]').querySelectorAll('a.recommended-thread-item')[1].href;
  //Source 2
  var leikiBoxContentSource2 = document.querySelector('[data-leiki-name="aller2"]').querySelectorAll('.recommended-thread-site')[1].innerText;

  //Headline 3
  var leikiBoxContentTitle3 = document.querySelector('[data-leiki-name="aller2"]').querySelectorAll('.recommended-thread-info .text-primary')[4].innerText;
  //Link 3
  var leikiBoxContentLink3 = document.querySelector('[data-leiki-name="aller2"]').querySelectorAll('a.recommended-thread-item')[2].href;
  //Source 3
  var leikiBoxContentSource3 = document.querySelector('[data-leiki-name="aller2"]').querySelectorAll('.recommended-thread-site')[2].innerText;

  //Add content 1
  document.querySelector('section').innerHTML += '<a href="'+leikiBoxContentLink+'"><div class="nativeExperimentBox"><b>'+leikiBoxContentTitle+'</b><span class="nativeExperimentSource" class="smaller">'+leikiBoxContentSource+'</span><p>Lue lisää</p></div></a>';

  //Add content 2
  document.querySelectorAll('section')[1].innerHTML += '<a href="'+leikiBoxContentLink2+'"><div class="nativeExperimentBox"><b>'+leikiBoxContentTitle2+'</b><span class="nativeExperimentSource" class="smaller">'+leikiBoxContentSource2+'</span><p>Lue lisää</p></div></a>';

  //Add content 2
  document.querySelectorAll('section')[2].innerHTML += '<a href="'+leikiBoxContentLink3+'"><div class="nativeExperimentBox"><b>'+leikiBoxContentTitle3+'</b><span class="nativeExperimentSource" class="smaller">'+leikiBoxContentSource3+'</span><p>Lue lisää</p></div></a>';

  //Styles
  var nativeBoxContainerStyles = 'border: 1px solid #d6d6d6; padding: 3% 2% 1%; margin: 1% 0 2% 0;';
  document.getElementsByClassName("nativeExperimentBox")[0].style = nativeBoxContainerStyles;
  document.getElementsByClassName("nativeExperimentBox")[1].style = nativeBoxContainerStyles;
  document.getElementsByClassName("nativeExperimentBox")[2].style = nativeBoxContainerStyles;

  var nativeBoxContainerTitle = 'text-align: left; width: 80%; color: #000; display: inline-block;';
  document.getElementsByClassName("nativeExperimentBox")[0].querySelector('b').style = nativeBoxContainerTitle;
  document.getElementsByClassName("nativeExperimentBox")[1].querySelector('b').style = nativeBoxContainerTitle;
  document.getElementsByClassName("nativeExperimentBox")[2].querySelector('b').style = nativeBoxContainerTitle;

  var nativeBoxSourceStyles = "text-align: right; display: inline-block; width: 20%; vertical-align: top;";
  document.getElementsByClassName('nativeExperimentSource')[0].style = nativeBoxSourceStyles;
  document.getElementsByClassName('nativeExperimentSource')[1].style = nativeBoxSourceStyles;
  document.getElementsByClassName('nativeExperimentSource')[2].style = nativeBoxSourceStyles;

  var nativeBoxParagraph = "margin: 5px 0 5px;";
  document.getElementsByClassName('nativeExperimentBox')[0].querySelector('p').style = nativeBoxParagraph;
  document.getElementsByClassName('nativeExperimentBox')[1].querySelector('p').style = nativeBoxParagraph;
  document.getElementsByClassName('nativeExperimentBox')[2].querySelector('p').style = nativeBoxParagraph;
}
else {
  console.log('the element does not exist');
}
