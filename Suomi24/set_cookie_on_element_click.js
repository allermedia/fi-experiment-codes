/*
Domain: Suomi24.fi
Experiment name: Set cookie on element click
Version: 1.0
Platform: -
Author: Thomas
*/

//Set cookie on click
[anySelectorHere].addEventListener("click", function(){
    //Do other stuff here if needed

    //Set cookie valid for a month
    var d = new Date();
    d.setTime(d.getTime() + (30*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = "s24App=1;"+ expires + ";path=/";
});
