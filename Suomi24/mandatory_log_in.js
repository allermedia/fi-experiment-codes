/*
Domain: Suomi24.fi
Experiment name: Mandatory log in 
Version: 1.0
Platform: VWO
Author: Pan

*/



// Only when user is not logged in 
if($("#s24-navbar-collapse-1 strong").length > 1 && $("#s24-navbar-collapse-1 strong")[0].innerHTML == "KIRJAUDU") {

    // hide the orginal button
    hideOrginalBtn(true);

    let heading = "Would you like to sign in or register to start a new topic?";

    let btn_login = "Kirjadu";
    let btn_register = "Rekisterödy";
    let btn_continue = "Continue as Anonmyous User";
    let txt_thank = "Thank for your help, you can use the keskustelu service as normal"; // ??
    let lnk_createTopic = "https://keskustelu.suomi24.fi/t/uusi";
        
    doModal(heading);

}   


function insertButtons (html, buttons) {

    html += '<div class="row" style="margin-top: 1rem;">';
    html += '<div class="col-lg-12 text-center">';
    html += '<a id="" class="btn btn-primary" href=""> r</a>';
    html += '</div>';
    html += '</div>';
}

function hideOrginalBtn(fake = false) {
    if (fake) {
        $(".topic-description+a").addClass("hidden");

        // create a button to replace the orginal one
        let btn = "<a id='btn-modal' class='btn btn-primary' href='#'>ALOITA UUSI KESKUSTELU</a>";
        $(btn).insertAfter($(".topic-description"));
    }
    else {
        if ($("#btn-modal").length > 0) {
            $("#btn-modal").remove();
        }

        $(".topic-description+a").removeClass("hidden");
    }
}

function doModal(heading, formContent) {
    html =  '<div id="dynamicModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="confirm-modal" aria-hidden="true">';
    html += '<div class="modal-dialog">';
    html += '<div class="modal-content">';
    html += '<div class="modal-header">';
    html += '<a class="close" data-dismiss="modal">×</a>';
    html += '<h4>'+heading+'</h4>'
    html += '</div>';
    html += '<div class="modal-body">';
    html += formContent;
    html += '</div>';  // body
    html += '</div>';  // content
    html += '</div>';  // dialog
    html += '</div>';  // modalWindow
    $('body').append(html);
    $("#dynamicModal").modal();
    $("#dynamicModal").modal('show');

    // restore the orginal button if user closed the dialog
    $('#dynamicModal').on('hidden.bs.modal', function (e) {
        $(this).remove();
        hideOrginalBtn();
    });

}

// <div id="myModal" class="modal fade" role="dialog">
//   <div class="modal-dialog">

//     <!-- Modal content-->
//     <div class="modal-content">
//       <div class="modal-header">
//         <button type="button" class="close" data-dismiss="modal">&times;</button>
//         <h4 class="modal-title" style="text-align: center;" >Download on app store</h4>
//       </div>
//       <div class="modal-body">
//         <div class="row">
//           <div class="col-lg-12"><img src="https://s12.postimg.org/fmzk1vsi5/phylo.png" class="img-responsive" /></div>
//         </div>
//         <div class="row">
//           <div class="col-xs-4 col-xs-offset-2"><img src="https://s17.postimg.org/jjwynz1en/ios.png" class="img-responsive" /></div>
//           <div class="col-xs-4"><img src="https://s22.postimg.org/mj975ck6p/android.png" class="img-responsive" /></div>
//         </div>
        
//       </div>
//       <!--<div class="modal-footer">
//         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
//       </div>-->
//     </div>

//   </div>
// </div>