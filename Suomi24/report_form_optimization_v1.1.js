/*
Domain: Suomi24.fi
Experiment name: Report form optimization v1
Version: 1.0
Platform: VWO
Author: Pan
*/


// when the report form is open
$(".thread").on("click", ".action-bar-report", function(){
    let txt_text_only_input_header = "Anna ilmiannon syy";  
    let txt_text_only_input_placeholder = "Kirjoita tähän"; 
    let txt_radio_other_reason = "Muu syy";
    let txt_checkbox_donot_send_email ="Alä lähetä sähköpostiani ylläpidolle."
    let txt_headline_direct_submit = "Valitse yksi vaihtoehdoista";
    let form_report = $(".action-bar-dropdown.report.open form");
    let input_text= form_report.find(".action-bar-dropdown-container .form-group:not(.captcha-img-container):nth-of-type(1)");
    let input_email_wrapper= form_report.find(".action-bar-dropdown-container .form-group:not(.captcha-img-container):nth-of-type(2)");
    //let input_email= form_report.find(".action-bar-dropdown-container .form-group:not(.captcha-img-container):nth-of-type(2)").find("input.report-email");
    let line_break = form_report.find("br");
    let radio_input_group = form_report.find(".radio-button-group");
    let radio_other_reason = '<div id="btn_reason_other" class="radio other"><label class="radio-inline">'
                            + '<input type="radio" name="report-type" value="outofsubject"><span>'
                            + txt_radio_other_reason 
                            + '</span></label></div>';
    
    let isDesktop = false;
    if ( window.innerWidth > 768 ) {
        isDesktop = true;
    } 

    // TASK 1: "Send report without email" 
    if ($("#s24-navbar-collapse-1 strong").length == 1 && $("#s24-navbar-collapse-1 strong")[0].innerHTML !== "KIRJAUDU") {
        // email input
        let html_input_email = input_email_wrapper.html();
        let mergin_top = isDesktop ? ' style="margin-top: 32px;"':'';


        // checkbox do not send my email
        let checkbox_donot_send_email = '<div class="col-md-6"><div class="checkbox"'
                                        +  mergin_top//Desktop styles
                                        + '>' 
                                        + '<label class="email-label">' 
                                        + '<input id="checkbox_donot_send_email" type="checkbox" name="not-required"><span>'
                                        + txt_checkbox_donot_send_email
                                        + '</span></label></div></div>'; 

        let html_new = '<div class="row"><div class="col-md-6">'
                    + html_input_email
                    + '</div>' // class="col-md-6"
                    + checkbox_donot_send_email
                    +'</div>'; // class="row"

        input_email_wrapper.html(html_new);

        // when checkbox is checked: disable the input
        let input_email= form_report.find(".action-bar-dropdown-container .form-group:not(.captcha-img-container):nth-of-type(2)").find("input.report-email");
        let val_input_email = input_email.val();

        form_report.on('click','#checkbox_donot_send_email', function() {
            
            if($("#checkbox_donot_send_email").prop("checked")) {
                input_email.prop("disabled", true);
                input_email.val("");
            }
            else {
                input_email.prop("disabled", false);
                input_email.val(val_input_email);
            }
        }); 
    }

    // TASK2: Use only text input
    // Check the last one by default
    radio_input_group.find(input[value="outofsubject"])
    radio_input_group.addClass("hidden"); // hide the radio button group

    input_email_wrapper.addClass("hidden");
    input_text.find("label").html(txt_text_only_input_header);
    input_text.find('input').attr("placeHolder",txt_text_only_input_placeholder);
    line_break.remove();

    // TASK3: Use Else reason
    radio_input_group.append(radio_other_reason);
    // form_report.on('click', '.other', function() {
    //     //if( checked)
    //     input_text.removeClass("hidden");
    //     // else 

    //     input_text.addClass("hidden");
    // });

    // TASK4: submit report on click 
    // when user is logged in
    if ($("#s24-navbar-collapse-1 strong").length > 1 && $("#s24-navbar-collapse-1 strong")[0].innerHTML == "KIRJAUDU") {
        radio_input_group.append(radio_other_reason);
        let radio_selections = $(".radio-button-group > div");
       
        for (let i = 0; i < radio_selections.length; i++) {

             // Add hover effects
            $(radio_selections[i]).mouseenter(function() {
                $(this).css("background", "#F6F6F4").css("border-radius", "3px");
            }).mouseleave(function() {
                $(this).css("background", "#FFF").css("border-radius", "0px");
            });
            // hide the info button and radio button
            $(radio_selections[i]).find("a").addClass("hidden");
            $(radio_selections[i]).find(".radio-inline span").addClass("hide_radio_button");

            // Add submit event handler
            $(radio_selections[i]).click(function() {
                form_report.submit();
            })

        }
        // Add headline to the form: 
        form_report.prepend("<h3>"+ txt_headline_direct_submit + "</h3>");
        $(".action-bar-dropdown-container").addClass("hidden");

    }


});


//  TASK 1: "Send report without email" in VWO
// if the user is log in
if ($("#s24-navbar-collapse-1 strong").length == 1 && $("#s24-navbar-collapse-1 strong")[0].innerHTML !== "KIRJAUDU") {  
  
    console.log("script is here");
    
    
    let mergin_top =  (window.innerWidth > 768) ? ' style="margin-top: 32px;"':'';
    let txt_checkbox_donot_send_email ="Alä lähetä sähköpostiani ylläpidolle.";
    
    // checkbox do not send my email
    let checkbox_donot_send_email = '<div class="col-md-6"><div class="checkbox"'
      +  mergin_top //Desktop styles
      + '>' 
      + '<label class="email-label">' 
      + '<input id="checkbox_donot_send_email" type="checkbox" name="not-required"><span>'
      + txt_checkbox_donot_send_email
      + '</span></label></div></div>'; 
      
        val_input_email = "Email";
            
      $('body').on('click','#checkbox_donot_send_email', function() {
        if($("#checkbox_donot_send_email").prop("checked")) {
          $(".report-form input.report-email").prop("disabled", true);
          $(".report-form input.report-email").val("");
        }
        else {
          $(".report-form input.report-email").prop("disabled", false);
          $(".report-form input.report-email").val(val_input_email);
        }
      });
    
    $(".thread").on("click", ".action-bar-report", function(){
      
      console.log("click");
      val_input_email = $(".report-form input.report-email").val();
      console.log(val_input_email);
      
      let html_input_email = vwo_$(".report-form .form-group:not(.captcha-img-container):nth-of-type(2)").html();  
      let html_new = '<div class="row"><div class="col-md-6">'
        + html_input_email
        + '</div>' // class="col-md-6"
        + checkbox_donot_send_email
        +'</div>'; // class="row"
  
      $(".report-form .form-group:not(.captcha-img-container):nth-of-type(2)").html(html_new);
      
    });
    
  }
  

// TASK2: Use only text input in VWO
//Remove e-mail field
let email_input = $('.report-email');
email_input.addClass("hidden");
//Remove input headlines
$('.report-form .input-header').remove();
//Set new headline
$('.action-bar-dropdown-container').prepend('<h3 id="headline">Anna ilmiannon syy</h3>');
//Delete br
$('.report-form br').remove();
//Placeholder text
$('.report-form .report-body').attr('placeholder','Kirjoita tähän');
// Select the last option by default as a mock up
$("form input[value='outofsubject']").prop('checked', true);
//hide radio button containers (report reasons)
$('.radio-button-group').addClass('hidden');

// TASK3 with other reason in VWO
let txt_radio_other_reason = "Muu syy";
let radio_other_reason = '<div id="btn_reason_other" class="radio other"><label class="radio-inline">'
  + '<input type="radio" name="report-type" value="outofsubject"><span>'
  + txt_radio_other_reason 
  + '</span></label></div>';
$('.radio-button-group').append(radio_other_reason);   

console.log("script is here");

// hide the text input and remove the line break
$(".report-form .action-bar-dropdown-container .form-group.has-feedback:not(.captcha-img-container)").addClass("hidden").css("margin:0;");
$(".report-form .action-bar-dropdown-container br").remove();   
$(".report-form .action-bar-dropdown-container").css("padding-top", 0);


// TASK: click on radion button to submit form
// when user is logged in
if ($("#s24-navbar-collapse-1 strong").length === 1 && $("#s24-navbar-collapse-1 strong")[0].innerHTML !== "KIRJAUDU") {
  
    console.log("script is here");
    
    let txt_radio_other_reason = "Muu syy";
    let radio_other_reason = '<div id="btn_reason_other" class="radio other"><label class="radio-inline">'
      + '<input type="radio" name="report-type" value="outofsubject"><span>'
      + txt_radio_other_reason 
      + '</span></label></div>';
    $('.radio-button-group').append(radio_other_reason);
    
    // Add submit event handler
    $('body').on('click', '.radio-button-group > div.radio', function() {
      console.log("clicked");
      $(".report-form .btn-submit-report").click();
    } );
    
    let radio_selections = $(".radio-button-group > div.radio");
    for (let i = 0; i < radio_selections.length; i++) {
      // Add hover effects
      $(radio_selections[i]).mouseenter(function() {
        $(this).css("background", "#F6F6F4").css("border-radius", "3px");
      }).mouseleave(function() {
        $(this).css("background", "#FFF").css("border-radius", "0px");
      });
      // hide the info button and radio button
      $(radio_selections[i]).find("a").addClass("hidden");
    }
    // Add headline to the form: 
    $(".report-form form").prepend("<h3>Valitse yksi vaihtoehdoista</h3>");
    $(".action-bar-dropdown-container").addClass("hidden");
  }
  