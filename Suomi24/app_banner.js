/*
Domain: Suomi24.fi
Experiment name: Suomi24 application promotion experiment
Version: 1.0
Platform: VWO
Author: Thomas
*/

//Application banner content
var bannerContainerStart = "<div id='applicationBanner'>";
var bannerIcon = "<img id='bannerIcon' src='https://www.suomi24.fi/img/logos/admin_highlight.svg'>";
var bannerClose = "<span id='bannerClose'>&#10005;</span>";
var bannerStars = '<div id="bannerStars"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><span id="bannerReviewCount">(1540 reviews)</span></div>';
var bannerText = "<div id='bannerParagraph'>Koko Suomi taskussasi</div>";
var bannerHeadline = "<b id='bannerHeadline'>Suomi24 Keskustelu</b>";
var bannerCTA = "<div id='bannerCTA' class='btn btn-primary btn-sm'>Lataa sovellus</div>";
var bannerContainerEnd= "</div>";
var applicationBanner = bannerContainerStart+bannerClose+bannerIcon+bannerHeadline+bannerCTA+bannerText+bannerStars+bannerContainerEnd;

//Main page container
var mainContainer = document.getElementsByClassName('main-container')[0];

//Cookie policy box check
var cookiePolicyBox = document.getElementsByClassName('s24_cc_banner-wrapper').length === 0;
if ( cookiePolicyBox ) {
    $('body').prepend(applicationBanner);
}

//Styles
var bannerContainerStyles = document.getElementById('applicationBanner');
var bannerIconStyles = document.getElementById('bannerIcon');
var bannerHeadlineStyles = document.getElementById('bannerHeadline');
var bannerTextStyles = document.getElementById('bannerParagraph');
var bannerCTAStyles = document.getElementById('bannerCTA');
var bannerStarContainerStyles = document.getElementById('bannerStars');
var bannerStarsStyles = document.querySelectorAll('#bannerStars i');
var bannerCloseStyles = document.getElementById('bannerClose');
var bannerReviewCountStyles = document.getElementById('bannerReviewCount');

bannerContainerStyles.style = "margin: 2% 0; background: #fff; padding: 2%;";
bannerIconStyles.style = "width: 75px; background: white; float:left; padding: 10px 12.5px; margin: 0 10px; border: 1px solid #cbcbcb; border-radius: 20px;";
bannerHeadlineStyles.style = "font-size: 1.1em; margin: 10px 0;";
bannerTextStyles.style = "font-size: .9em; color: #acacac;";
bannerCloseStyles.style = "display: block; float:left; font-size: 1.2em; padding: 20px 10px 10px 0;";
bannerStarContainerStyles.style = "display: block; color: #FEAF50; margin: 2.5px 0 10px;";
bannerCTAStyles.style = "float: right; background: #76c932; box-shadow: 0 6px 0 #57a01c; border: 1px solid #76c932; margin: 0 10px 0 0; position: relative; top: 10px;";
bannerReviewCountStyles.style = "font-size: .6em; color: #c0c0c0;";

for ( var i=0; i < 4; i++ ) {
  bannerStarsStyles[i].style = "margin: 0 2px 0 0;";
}

//Functionality on close button click
bannerCloseStyles.addEventListener("click", function(){
    bannerContainerStyles.style = "display: none;";
    //Set cookie valid for a month
    var d = new Date();
    d.setTime(d.getTime() + (30*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = "s24App=1;"+ expires + ";path=/";
});

//Functionality on CTA button click
bannerCTAStyles.addEventListener("click", function(){
    bannerCTAStyles.style = "display: none;";
    bannerIconStyles.style = "display: none;";
    bannerCTAStyles.style = "display: none;";
    bannerTextStyles.style = "display: none;";
    bannerStarContainerStyles.style = "display: none;";
    bannerHeadlineStyles.outerHTML = "<p id='bannerHeadline'><b>Mahtavaa, että kiinnostuit Suomi24-sovelluksesta!</b><br/> Sovellus on vielä työn alla, mutta kerro meille, millainen sen pitäisi olla.<br/><div id='bannerCTA' class='btn btn-primary btn-sm surveyLink'>Vastaa kyselyyn (1 min.)</div></p>";
    $('#bannerCTA').css("margin","0 0 0 25px");
    //Set cookie valid for a month
    var d = new Date();
    d.setTime(d.getTime() + (30*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = "s24App=1;"+ expires + ";path=/";

    //Go to survey
    var surveyLink = document.getElementsByClassName('surveyLink')[0];
    surveyLink.addEventListener("click", function(){
      window.location.href = "https://insights.hotjar.com/s?siteId=718377&surveyId=57843";
    });

});

//Small mobile screens
if ( window.innerWidth < 435 ) {
  $('#bannerCTA').insertAfter('#bannerStars');
  bannerCTAStyles.style.float = "none";
  bannerCTAStyles.style.margin = "0 0 5px 0";
  bannerCTAStyles.style.top = "0px";
  bannerCTAStyles.style.left = "120px";
}
