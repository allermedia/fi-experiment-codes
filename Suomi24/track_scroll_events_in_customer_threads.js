/*
Domain: Suomi24.fi
Experiment name: Event when scrolling past customer thread answer
Version: 1.0
Platform: -
Author: Thomas
*/

//Add scroll function
window.addEventListener('scroll',function scrollFunction(){
  //Location of element
  var elementLocation  = document.getElementsByClassName('autokeskus')[0].offsetTop;
  //If element is passed
  var scrollLocation = document.getElementsByTagName('body')[0].scrollTop;
  //Trigger stuff when location is passed
  if ( elementLocation < scrollLocation) {
    //Add tracking event code here
    console.log('Scrolled past Autokeskus discussion thread.');
    window.removeEventListener('scroll', scrollFunction);
  }
});
