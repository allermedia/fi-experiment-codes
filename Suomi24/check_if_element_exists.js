/*
Domain: Suomi24.fi
Experiment name: Check if element exists
Version: 1.0
Platform: -
Author: Thomas
*/

//Define element
var elementExists = document.getElementsByClassName("sponsored-ad")[0];

//Check if element exists
if ( document.body.contains(elementExists) == true) {
  console.log('the element exists');
} else {
  console.log('the element does not exist');
}
