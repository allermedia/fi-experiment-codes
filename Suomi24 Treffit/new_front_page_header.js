/*
Domain: Treffit.suomi24.fi
Experiment name: New front page header experiment code
Version: 1.0
Platform: VWO
Author: Thomas
*/

//Content variables for header / splash screen
var splashLogo = '<img class="img-responsive subscription-plus-logo" src="https://treffit.suomi24.fi/img/logos/treffit24_plus.png" alt="Treffit Plus" style="margin: 2% auto;">';
var splashContentBox1 = '<div class="col-xs-12 col-sm-4"><img src="/img/content/treffit_kuvat-01.png" class="img-responsive" style="max-width: 100px; margin:0 auto;"><p class="splashParagraph">Liittyminen kestää vain pari minuuttia.</p></div>';
var splashContentBox2 = '<div class="col-xs-12 col-sm-4"><img src="/img/content/treffit_suurennuslasi-01.png" class="img-responsive" style="max-width: 100px; margin:0 auto;"><p class="splashParagraph">Kymmenet tuhannet ovat jo löytäneet täältä toisensa.</p></div>';
var splashContentBox3 = '<div class="col-x-s12 col-sm-4"><img src="/img/content/treffit_lintu-01.png" class="img-responsive" style="max-width: 100px; margin:0 auto;"><p class="splashParagraph">Keskimäärin 60 000 viestiä lähetetään päivässä.</p></div>';
var splashTitle = '<h1 id="splashHeadline" class="col-xs-12">TÄÄLTÄ LÖYDÄT <span id="counter"><u>72250</u></span> MUUTA IHMISTÄ!</h1>';
var splashCTA = '<div class="col-xs-12"><a href="/profile/create" class="btn-green splashCTA">Liity ilmaiseksi nyt</a></div>';
var splashLoginText = '<b id="splashLogin">Oletko jo jäsen? <a>Kirjaudu tästä</a></b>';

//Add contents to splash screen
document.getElementById('splash-screen').innerHTML = "<div id='splash-screen'>"+splashLogo+splashTitle+splashContentBox1+splashContentBox2+splashContentBox3+splashCTA+splashLoginText+"</div>";

//Style variables
var splashScreen = document.getElementById('splash-screen');
var splashHeadline = document.getElementById('splashHeadline');
var splashImages = document.getElementsByClassName('img-responsive');
var splashParagraph = document.getElementsByClassName('splashParagraph');
var splashCTAButton = document.getElementsByClassName('splashCTA')[0];
var splashLoginCTA = document.getElementById('splashLogin');

//Styles
splashScreen.style.margin = "2% auto";
splashScreen.style.textAlign = "center";
splashHeadline.style.margin = "1.5% 0 2% 0";
splashCTAButton.style = "margin: 25px auto; background: #98C04D; padding: 20px; color: #fff; border-radius: 4px; text-transform: uppercase; display: block; max-width: 350px;";
splashLoginCTA.style.cursor = "pointer";

for (i = 0; i < splashParagraph.length; i++ ) {
  splashParagraph[i].style.fontSize = "1.5em";
  splashParagraph[i].style.lineHeight = "1.33em";
  splashParagraph[i].style.padding = "5%";
}

//Log in click event to show log in fields
document.getElementById('splashLogin').addEventListener("click", function(){
  document.getElementsByTagName('ul')[0].getElementsByTagName('a')[1].click();
});

//Profile amount counter in header
var counter = 72250;
var j = setInterval(function(){
  counter++;
  if ( counter >= 72285 ) {
    clearInterval(j);
    var k = setInterval(function(){
      counter++;
      document.getElementById('counter').innerHTML = "<u>"+counter+"</u>";
      if ( counter === 72289 ) {
        clearInterval(k);
      }
    }, 500 );
  }
  document.getElementById('counter').innerHTML = "<u>"+counter+"</u>";
}, 100 );
