/*
Domain: Treffit.suomi24.fi
Experiment name: Mobile sign in UX improvement (show password button)
Version: 1.0
Platform: VWO
Author: Thomas
*/

//Variable for password field
var userInputPw = document.querySelectorAll('#login-form .form-group')[1];

//Append "Näytä" button
var content = document.createElement('div');
content.id = 'showPwLink';
content.innerHTML = "Näytä";
userInputPw.appendChild(content);

//Styles
var showPwLink = document.getElementById('showPwLink');
showPwLink.style = "float: right; position: relative; top: -24px; color: #000; margin: 0 10px 0 0;font-size: 0.8em; text-transform: uppercase; font-weight: bold;";

//Show password on click
showPwLink.addEventListener('click',function(){

  //Variable for password input value
  var userInputPwValueType = document.querySelector('#login-form #password');
  if ( userInputPwValueType.type === "text" ) {
    userInputPwValueType.setAttribute("type","password");
  }
  else if ( userInputPwValueType.type === "password" ) {
    userInputPwValueType.setAttribute("type","text");
  }
});
