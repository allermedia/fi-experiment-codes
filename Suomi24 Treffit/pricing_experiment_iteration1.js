/*
Domain: Treffit.suomi24.fi
Experiment name: Simple Treffit product page experiment
Version: 1.0
Platform: Optimizely
Author: Thomas
*/

//Variation 1 (show two month product and hide continuous subscription)
document.querySelector('#two_month_subscription').classList.remove("hidden");
document.querySelector('#continuous_subscription').style.display = "none";

//Variation 2 (show three month product and hide continuous subscription)
document.querySelector('#three_month_subscription').classList.remove("hidden");
document.querySelector('#continuous_subscription').style.display = "none";
