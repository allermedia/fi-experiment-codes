/*
Domain: Treffit.suomi24.fi
Experiment name: Treffit product page optimization
Version: 1.5-ish
Platform: Optimizely
Author: Thomas
*/

//Remove header content
$('.plus-promo div.plus-header-2').remove();

//Add product box container
$('.plus-promo').prepend('<div id="productContainer"></div>');

//Add products to container
$('.kd-subscription-container').appendTo('#productContainer');

//Remove startNumber
$('.div-star-icon').remove();

//Remove shout icon
$('.icon-large').parent().parent().remove();

//Remove subscription textarea
$('.subscription-text').remove();

//Remove sub name and price
$('.subscription-plus-name').parent().remove();
$('.subscription-plus-price').parent().remove();

//Payex content and styles
$('.span12 p').text('Maksutiedot välittää: ');
$('.plus-promo .payex-logo').css('max-width','75px');
$('.span12 p').css({
  'display' : 'inline',
  'margin' : '0 10px 0 0'
});

//Add compelling texts to products
$('.kd-subscription-container:eq(0)').prepend('<div class="box"><div class="ribbon">Suosituin</div></div><p style="visibility: hidden;">Treffit Plus</p><b>KESTOTILAUS</b><p class="productText">Tutustu deittikumppaneihin paremmin ja pintaa syvemmältä.</p><div class="price"><span>12</span>,95</div> €/kk');
$('.kd-subscription-container:eq(1)').prepend('<p style="visibility: hidden;">Treffit Plus</p><b>2 VIIKKOA</b><p class="productText">Tutustu erinomaisiin etuihin ja pistä verkot vesille jo tänään.</p><div class="price"><span>9</span>,95</div> €');
$('.kd-subscription-container:eq(2)').prepend('<p style="visibility: hidden;">Treffit Plus</p><b>1 KUUKAUSI</b><p class="productText">Tutustu deittikumppaneihin tehokkaasti kuukauden ajan.</p><div class="price"><span>14</span>,95</div> €');

//Add Treffit Plus image to header
$('.plus-promo').prepend('<img class="img-responsive subscription-plus-logo" src="https://treffit.suomi24.fi/img/logos/treffit24_plus.png" alt="Treffit Plus">');

//UVP text after products
$('<div id="UVP"><h2>Löydä seuraa <b>54%</b> varmemmin kuin treffailijat ilman plussapakettia</h2><p>(Käyttäjäkartoitus, 2015)</p></div>').insertAfter('#productContainer');
//Feature list after UVP element
$('<div id="featureList"></div>').insertAfter('#UVP');

//Treffit Plus feature promotion
//Feature 1
$('#featureList').append('<div class="feature_box"><img src="https://treffit.suomi24.fi/uploads/contentpicture/origin/cms/20160420092511000000.png"><b>Viestittele rennommin</b><p>Hyvästi kurjat viestirajoitukset, Treffit Plus -käyttäjänä saat lähettää viestejä mielinmäärin joka päivä! Mahdollisuutesi hyviin kontakteihin paranevat merkittävästi ja kumppanin löytyminen on entistä helpompaa.</p></div>');

//Feature 2
$('#featureList').append('<div class="feature_box"><img src="https://treffit.suomi24.fi/uploads/contentpicture/origin/cms/20160420092519000000.png"><b>Näe faktat</b><p> Ketkä sopivat juuri sinulle? Kuka vieraili profiilissasi? Kenen suosikki olet? Treffit Plus -käyttäjänä saat lisätietoa myös toisten käyttäjien tilastoista.</p></div>');

//Feature 3
$('#featureList').append('<div class="feature_box"><img src="https://treffit.suomi24.fi/uploads/contentpicture/origin/cms/20160420092539000000.png"><b>Paranna mahdollisuuksiasi</b><p>Tuplaa todennäköisyytesi rakkauteen! Treffit Plus -käyttäjänä näyt hakutuloksissa ennen muita ja saat oman profiilisi korostetusti esiin megafonihuudoilla omaan hakuusi sopiville henkilöille.</p></div>');

//Focus to the middle
$('.kd-subscription-container:eq(0)').insertAfter('.kd-subscription-container:eq(1)');

//Remove stuff in popup
$('h4:contains(Maksu), .discount-code').remove();

//Styles to Maksutapa element
$('label:contains("Maksutapa *")').next().show().css('width','50%');
$('label:contains("Maksutapa *")').next().show().css('margin','1% auto');

//Styles
$('img.subscription-plus-logo').css({
  'margin' : '2% auto'
});

$('#productContainer').css({
  'text-align' : 'center'
});

$('.kd-subscription-container').css({
  'width' : '20%',
  'display' : 'inline-block',
  'margin' : '1em',
  'font-size' : '1.1em',
  'background' : '#fff',
  'color' : '#000',
  'border-radius' : '10px'
});

$('.background-pink').css({
  'border' : '2.5px solid #E1A1C3'
});

$('.background-green').css( {
  'border' : '5px solid #98C04D'
});

$('.background-purple').css( {
    'border' : '2.5px solid #E1A1C3'
});

$('.kd-subscription-container b').css({
  'font-size' : '2em',
  'display' : 'block'
});

$('.kd-subscription-container .price').css({
  'color': 'green',
  'font-size' : '1.5em',
  'font-weight' : 'bold',
  'display' : 'inline'
});

$('.kd-subscription-container .price span').css({
  'font-size' : '2.5em'
});

$('.kd-subscription-container p.productText').css({
  'width' : '80%',
  'margin' : '4% auto',
  'color' : '#a7a7a7'
});

$('.subscription-buttons').removeClass('col-sm-3');

$('.product-detail-view-button').css({
  'width' : '100%',
  'height' : '50px',
  'font-size' : '1.33em'
});

$('#agreement-info').css({
  'width' : '100%',
  'display' : 'block',
  'color' : '#000',
  'margin' : '5% 0'
});

$('#UVP').css({
  'text-align' : 'center',
  'width' : '40%',
  'margin' : '2.5% auto 0'
});

$('#featureList').css({
  'text-align' : 'center'
});

$('.feature_box').css({
  'display' : 'inline-block',
  'width' : '20%',
  'margin' : '1% 0',
  'padding' : '2%',
  'vertical-align' : 'text-top'
});

$('.feature_box b').css({
  'display' : 'block',
  'font-size' : '1.5em',
  'margin' : '10px 0 20px 0'
});

$('#UVP b').css({
  'color' : 'green',
  'font-size': '2em',
  'display': 'block',
  'padding' : '2.5% 0',
  'font-style' : 'normal'
});

$('#UVP h2').css( {
  'font-family' : 'Lato, sans-serif',
  'font-style' : 'italic',
  'line-height' : '38px',
  'font-size' : '2em'
});

//Ribbon styles
$('.ribbon').css({
  'padding' : '.3% 1%',
  'background' : '#98c04d',
  'color' : '#fff',
  'position' : 'absolute',
  'margin' : '-25px 0 0 -1px',
});

$('.order-summary, .collapse .padded').css({
  'width' : '100%',
  'padding' : '0'
});

//Mobile styles
if ( window.innerWidth < 1000 ) {
  $('.kd-subscription-container').css({
    'width' : '90%'
  });

  $('#UVP').css({
    'width' : '80%'
  });

  $('.price').css({
    'font-size' : '2em'
  });

  $('.feature_box').css({
    'width' : '50%',
    'margin' : '1% auto',
    'display' : 'block'
  });

  $('.ribbon').css('padding','3% 5%');

  $('form button.btn-green, form button.btn-primary').css({
    'width': '70%',
    'height' : '70px',
    'margin': '1% auto 5%',
    'font-size' : '2em',
    'display': 'block',
    'padding' : '0px 0 0 0'
  });
}
