/*
Domain: Treffit.Suomi24.fi
Experiment name: Profile registration chatbot experiment
Version: 1.0
Platform: -
Author: Martin, Bhanu, Ruchi, Thomas
*/

vwo_$.getScript( "https://cdn.jsdelivr.net/gh/space10-community/conversational-form@0.9.71/dist/conversational-form.min.js" , function ( data, textStatus, jqxhr ) {
  var $body = $('body');
  var robotInfoHTML = '<div class="row padded"><div id="register-bot-container"><h1>UUTTA! Kokeile avustettua rekisteröintiä!</h1><p><img src="https://treffit.suomi24.fi/img/content/shout_default_robot.jpg">Deitti-botti auttaa sinua rekisteröitymään.<a href="https://insights.hotjar.com/s?siteId=716829&surveyId=37661"> Anna palautetta</a></p><button id="register-bot-button" class="btn btn-primary">Kokeile</button><div id="cf-context" role="cf-context" cf-context style="display: none;"></div></div></div><b id="or">tai ...</b>';
  var $mainContainer = $($('.main-container .container-fluid')[1]);
  $mainContainer.prepend(robotInfoHTML);
  kd.analytics.send('event', 'Experiments', 'RegisterChatbot', 'Chatbot shown');
  var $cfContext = $('#cf-context');
  var $profileCreateForm = $('#profileCreateForm');
  var $registerBotButton = $('#register-bot-button');
  var conversationalForm;

  var $username = $('#ProfileCreateForm_username');
  $username.attr({
    'cf-questions': 'Hei, olen täällä auttamassa sinua :)&&Mitä käyttäjätunnusta haluat käyttää?',
    'cf-error': 'Käyttäjätunnus on käytössä, valitse toinen tunnus.'
  });
  var $password = $('#ProfileCreateForm_password');
  $password.attr({
    'cf-questions': 'Mitä salasanaa haluat käyttää? (Vähintään 8 merkkiä joista väh. yksi numero ja yksi kirjain.)',
    'cf-error': 'Salasana on liian lyhyt (vähintään 8 merkkiä).'
  });
  var $passwordConfirm = $('#ProfileCreateForm_password2');
  $passwordConfirm.attr({
    'cf-questions': 'Mahtavaa. Kirjoita salasana vielä kerran.',
    'cf-error': 'Salasanat eivät täsmää.'
  });
    $('#ProfileCreateForm_gender_0').attr('cf-questions', 'Mikä on sukupuolesi?');
  var $email = $('#ProfileCreateForm_email');
  $email.attr({
    'cf-questions': 'Mikä on sähköpostisi?',
    'cf-error': 'Virheellinen sähköposti. Anna sähköposti uudestaan.'
  });
  $('#ProfileCreateForm_year').attr('cf-questions', 'Minä vuonna olet syntynyt (esim. 1985)?');
  $('#ProfileCreateForm_month').attr('cf-questions', 'Minä kuukautena olet syntynyt (numerona esim. 5)?');
  $('#ProfileCreateForm_day').attr('cf-questions', 'Minä päivänä olet syntynyt (numerona esim. 12)?');
  $('#ProfileCreateForm_news_subscription').attr('cf-questions', 'Haluatko Suomi24:n uutiskirjeen, jotta pysyt ajan tasalla palveluun liittyvistä asioista? Paina ENTER jos et halua uutiskirjettä.');
  $('#ProfileCreateForm_marketing_permission').attr('cf-questions', 'Saako sinulle lähettää Suomi24 Treffien ja Suomi24:n sekä valikoitujen yhteistyökumppaneidemme upeita etuja ja tarjouksia? Paina ENTER jos et halua hyödyntää etuja.');
  $('#ProfileCreateForm_tos').attr('cf-questions', 'Oletko lukenut ja hyväksynyt käyttöehdot? Valitse ja paina ENTER.');

  function setupConversationalForm() {
    return new cf.ConversationalForm({
      formEl: document.getElementById('profileCreateForm'),
      context: document.getElementById('cf-context'),
      dictionaryData: {
        "input-placeholder": "Kirjoita vastauksesi tähän ...",
        "group-placeholder": "Valitse jokin seuraavista",
        "input-placeholder-error": "Syöttämäsi tieto on väärä",
        "user-reponse-missing-group": "Ei valintaa"
      },
      userImage: "https://treffit.suomi24.fi/img/profile_default.png",
      robotImage:"https://treffit.suomi24.fi/img/content/shout_default_robot.jpg",
      flowStepCallback: function(dto, success, error) {
        if (dto.tag.id === 'ProfileCreateForm_username') {
          function done(response) {
            if (response.data.available === true) {
              $username.trigger('keyup');
              return success();
            } else {
              return error();
            }
          }
          $.ajax({
            type: 'POST', // TODO CHANGE FOR PROD
            url: 'https://id.suomi24.fi/users/availability',
            data: JSON.stringify({ username: dto.text }),
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            success: done,
            failure: error
          });
        } else if (dto.tag.id === 'ProfileCreateForm_password') {
          if (dto.text.length >= 8) {
            return success();
          } else {
            return error();
          }
        } else if (dto.tag.id === 'ProfileCreateForm_password2') {
          if (dto.text === document.getElementById('ProfileCreateForm_password').value) {
            return success();
          } else {
            return error();
          }
        } else if (dto.tag.id === 'ProfileCreateForm_email') {
          if (dto.text.includes('@')) {
            return success();
          } else {
            return error();
          }
        } else if (dto.tag.id === 'ProfileCreateForm_year') {
          updateRealField(); // Sets the ProfileCreateForm_birthday_em_ input
          return success();
        } else if (dto.tag.id === 'ProfileCreateForm_tos') {
          return success();
        } else {
          return success();
        }
      },
      submitCallback: function() {
        toggleRegisterBotChat();
        kd.analytics.send('event', 'Experiments', 'RegisterChatbot', 'Form submitted');
        $profileCreateForm.trigger('submit');
      }
    });

  }
  function toggleRegisterBotChat() {
    if (!conversationalForm) {
      conversationalForm = setupConversationalForm();
    }
    if ($cfContext.is(':visible')) {
      $cfContext.hide();
      $registerBotButton.text('Kokeile');
      kd.analytics.send('event', 'Experiments', 'RegisterChatbot', 'RegisterChatbot closed');
      conversationalForm.remove();
      conversationalForm = null;
    } else {
      $cfContext.show();
      kd.analytics.send('event', 'Experiments', 'RegisterChatbot', 'RegisterChatbot opened');
      $registerBotButton.text('Sulje');
      $profileCreateForm.trigger('click'); // Make captcha happy
    }
  }
  $registerBotButton.on('click', toggleRegisterBotChat);
});
