/*
Domain: Treffit.suomi24.fi
Experiment name: New front page header experiment code version 2
Version: 2.0
Platform: VWO
Author: Thomas
*/

//Content variables for header / splash screen
var splashLogo = '<img class="img-responsive subscription-plus-logo" src="https://treffit.suomi24.fi/img/logos/treffit24.svg" alt="Treffit Plus" style="margin: 2% auto;">';
var splashTitle = '<h1 id="splashHeadline" class="col-xs-12">TÄÄLTÄ LÖYDÄT <span id="counter"><u>72250</u></span> MUUTA IHMISTÄ!</h1>';
var splashCTA = '<div class="col-xs-12"><a href="/profile/create" class="btn-green splashCTA">Liity ilmaiseksi</a></div>';
var splashLoginText = '<b id="splashLogin">Oletko jo jäsen? <a>Kirjaudu tästä</a></b>';
var splashHeader = '<div id="splashSubHeadline">Treffeissä lähetetään jopa 60 000 viestiä päivittäin ja liittyminen kestää vain pari minuuttia.</div>';

//Add contents to splash screen
document.getElementById('splash-screen').innerHTML = "<div id='splash-screen'><div id='splashContent'>"+splashLogo+splashTitle+splashHeader+splashCTA+splashLoginText+"</div></div>";

//Style variables
var splashScreen = document.getElementById('splash-screen');
var splashHeadline = document.getElementById('splashHeadline');
var splashHeader = document.getElementById('splashSubHeadline');
var splashContent = document.getElementById('splashContent');
var splashCTAButton = document.getElementsByClassName('splashCTA')[0];
var splashLoginCTA = document.getElementById('splashLogin');

//Styles
splashScreen.style.margin = "0 auto 2% auto";
splashScreen.style.textAlign = "center";
splashScreen.style.maxWidth = "100%";
splashScreen.style.background = "url('https://images.pexels.com/photos/196666/pexels-photo-196666.jpeg') no-repeat";
splashScreen.style.backgroundSize = "cover";
splashScreen.style.backgroundPositionY = "-350px";

splashContent.style= "background: rgba(255,255,255,.8); width: 50%; margin: 2% auto; padding: 2% 0;";

splashHeadline.style.margin = "1.5% 0 2% 0";
splashSubHeadline.style = "font-size: 1.5em; width: 50%; margin: auto;";
splashCTAButton.style = "font-size: 1.25em; font-weight: bold; margin: 25px auto; background: #98C04D; padding: 20px; color: #fff; border-radius: 4px; text-transform: uppercase; display: block; max-width: 350px;";
splashLoginCTA.style.cursor = "pointer";


//Styles for header background image on certain device widths
if ( window.innerWidth < 1400 ) {
  splashScreen.style.backgroundPositionY = "-200px";
}

if ( window.innerWidth < 1000 ) {
  splashScreen.style.backgroundPositionY = "0px";
  splashContent.style.width = "100%";
}

//Log in click event to show log in fields
document.getElementById('splashLogin').addEventListener("click", function(){
  document.getElementsByTagName('ul')[0].getElementsByTagName('a')[1].click();
});

//Profile amount counter in header
var counterElement = document.getElementById('counter');
var counter = 72250;
var j = setInterval(function(){
  counter++;
  if ( counter >= 72285 ) {
    clearInterval(j);
    var k = setInterval(function(){
      counter++;
      counterElement.innerHTML = "<u>"+counter+"</u>";
      if ( counter === 72289 ) {
        clearInterval(k);
      }
    }, 500 );
  }
  counterElement.innerHTML = "<u>"+counter+"</u>";
}, 100 );
