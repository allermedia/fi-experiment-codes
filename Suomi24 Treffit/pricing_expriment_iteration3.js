/*
Domain: Treffit.suomi24.fi
Experiment name: Treffit pricing experiment iteration
Version: 3.0
Platform: Optimizely
Author: Thomas
*/

//Pricing experiment iteration 3
document.getElementById('three_month_subscription').style = "display: block !important;";
document.getElementById('three_month_subscription').querySelector('.text-center').style = "2.5px solid rgb(225, 161, 195) !important;";
document.getElementById('one_month_subscription').style = "display: none !important;";
document.getElementById('three_month_subscription').querySelector('.ribbon').style = "display: none;";

//Edit contents of products
document.getElementById('two_week_subscription').querySelector('h1').outerHTML = "<img src='https://s17.postimg.cc/y7piavcan/009-photo.png' width='80px'><br/><h4>Ensitreffit</h4><h1>2 VIIKKOA</h1>";
document.getElementById('continuous_subscription').querySelector('h1').outerHTML = "<br/><img src='https://s17.postimg.cc/uo3kl34fz/021-conversation.png' width='80px'><br/><h4>Tositarkoituksella</h4><h1>1 KUUKAUSI</h1><p>Jatkuva tilaus</p>";
document.getElementById('three_month_subscription').querySelector('h1').outerHTML = "<img src='https://s17.postimg.cc/nkvp5gran/018-heart-1.png' width='80px'><br/><h4>Tehopakkaus</h4><h1>3 KUUKAUTTA</h1>";

//Prices of products
document.getElementById('twoweeks_orig_price').innerHTML = '<span class="subscription-price-euro" id="twoweeks_euros">19</span><span id="twoweeks_cents">,90<span class="euro-mark" id="twoweeks_euro_mark"> €/kk</span></span><p class="kertaHinta">9,95€/ostos</p>';
document.getElementById('threemonths_orig_price').innerHTML = '<span class="subscription-price-euro" id="threemonths_euros">9</span><span id="threemonths_cents">,98<span class="euro-mark" id="threemonths_euro_mark"> €/kk</span></span><p class="kertaHinta">29,95€/ostos</p>';

//Edit styles for ribbon element
document.getElementById('continuous_subscription').querySelector('.ribbon').style= "position: relative !important; padding: 3% !important; text-transform: uppercase; ";

//Styles
document.getElementsByClassName('kertaHinta')[0].style = "color: #a7a7a7; font-size: .75em;";
document.getElementsByClassName('kertaHinta')[1].style = "color: #a7a7a7; font-size: .75em;";
document.querySelector('.btn-green').style.background = "#993366";
document.querySelector('.btn-green').style.borderColor = "#9E3A84";
document.querySelectorAll('.btn-primary')[1].style.background = "#98c14d";
document.querySelectorAll('.btn-primary')[1].style.borderColor = "#98c14d";
document.querySelectorAll('.kd-subscription-container')[0].style = "border: 2px solid #9e3a84;";
document.querySelectorAll('.kd-subscription-container')[3].style = "border: 2px solid #9e3a84;";

//Add images/icons to products
document.querySelectorAll('#featureList img')[0].outerHTML = "<img src='https://s17.postimg.cc/ghntpue5b/010-love-letter.png' height='80px'>;";
document.querySelectorAll('#featureList img')[1].outerHTML = "<img src='https://s17.postimg.cc/8ox5xul0v/003-wedding-day.png' height='80px'>;";
document.querySelectorAll('#featureList img')[2].outerHTML = "<img src='https://s17.postimg.cc/bvrphhkwf/015-hearts.png' height='80px'>;";

var subscriptionText = document.getElementsByClassName('subscription-text');
for (var i = 0; i < subscriptionText.length; i++) {
	subscriptionText[i].style = "padding-bottom: 0px !important; width: 70% !important; margin: 0 auto 1% !important;";
}

//Credits to footer
document.getElementsByClassName('aller-footer')[0].outerHTML += '<div id="iconCredit"><a id="creditLink" href="https://flaticon.com" target="_blank">Icons designed by Stockio from Flaticon</a></div>';
//Credit element variables
var credits = document.getElementById("iconCredit");
var creditLink = document.getElementById("creditLink");
//Styles for credits
credits.style.margin = "1% 0";
credits.style.textAlign = "center";
credits.style.fontSize = ".75em";
creditLink.style.color = "#dbdbdb";
