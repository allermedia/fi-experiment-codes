/*
Domain: Treffit.suomi24.fi
Experiment name: New front page footer experiment code
Version: 1.0
Platform: VWO
Author: Thomas
*/

//Footer content variables
var footerTitle = '<h2 class="col-xs-12">Haku päällä?</h2>';
var footerParagraph = "<p class='col-xs-12' id='footerParagraph'>Haku siirtyi kirjautumisen taakse ja nyt Treffit on entistä turvallisempi. Profiilin luominen on ilmaista ja nopeaa. Luo profiili ja pääset selailemaan profiileja tuota pikaa!</p>";
var footerCTA = '<div class="col-xs-12"><a href="/profile/create" class="btn-green splashCTA">Liity ilmaiseksi</a></div>';
var footerRegLink = '<b id="footerLogin">Oletko jo jäsen? <a>Kirjaudu tästä</a></b>';

//Append content to testimonials
var footer = document.getElementById('testimonials').innerHTML += "<div id='footer'>"+footerTitle+footerParagraph+footerCTA+footerRegLink+"</div>";

//Style variables
var footerContainer = document.getElementById('footer');
var footerCTA = document.getElementsByClassName('splashCTA')[1];
var footerParagraph = document.getElementById('footerParagraph');
var footerRegLinkStyles = document.getElementById('footerLogin');
//Styles
footerContainer.style= "margin: 2% 0";
footerCTA.style = "font-size: 1.25em; font-weight: bold; margin: 25px auto; background: #98C04D; padding: 20px; color: #fff; border-radius: 4px; text-transform: uppercase; display: block; max-width: 350px;";
footerParagraph.style = "padding: 0 10%; font-size: 1.5em; line-height: 36px;";
footerRegLinkStyles.style = "cursor: pointer";
