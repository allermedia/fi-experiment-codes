/*
Domain: Treffit.suomi24.fi
Experiment name: New front page testimonial code
Version: 1.0
Platform: VWO
Author: Thomas
*/

//Content header
var title = '<h1 class="col-xs-12">Treffailijat haluavat jakaa kokemuksiaan kanssasi</h1>';

//Testimonial contents
var content6 = '<div class="person col-md-3"><img class="faceImg" src="https://s17.postimg.cc/smjhlv2tb/smiling.png"><p class="quote">Löysin sielunkumppanin. Olen onnellinen ja niin on hänkin :D</p><h2 class="nickname">jozzukka </h2><b class="age">46v.</b> Uusimaa</div>';
var content5 = '<div class="person col-md-3"><img class="faceImg" src="https://s17.postimg.cc/ip8gssnhr/in-love.png"><p class="quote">Yksistä pienistä viattomista treffeistä tuli jotain ihan muuta...</p><h2 class="nickname">yourprincessnanna </h2><b class="age">19v.</b> Joensuu</div>';
var content4 = '<div class="person col-md-3"><img class="faceImg" src="https://s17.postimg.cc/smjhlv2tb/smiling.png"><p class="quote">Aivan ihanasti löytyi todellinen herrasmies. Elämä on ihanaa!</p><h2 class="nickname">radiorock </h2><b class="age">51v.</b> Satakunta</div>';
var content3 = '<div class="person col-md-3"><img class="faceImg" src="https://s17.postimg.cc/ip8gssnhr/in-love.png"><p class="quote">Löysin parhaan mahdollisen kumppanin Suomi24 Treffeiltä. :)</p><h2 class="nickname">Ninduliina </h2><b class="age">25v.</b> Pirkanmaa </div>';
var content2 = '<div class="person col-md-3"><img class="faceImg" src="https://s17.postimg.cc/6alosgo9r/happy-2.png"><p class="quote">Löysin kivan kaverin, jonka kanssa nyt treffailemme.</p><h2 class="nickname">Merkku12 </h2><b class="age">52v.</b> Pohjois-Savo</div>';
var content1 = '<div class="person col-md-3"><img class="faceImg" src="https://s17.postimg.cc/6alosgo9r/happy-2.png"><p class="quote">Ei menny kun pari päivää kun löysin täältä sopivan kumppanin...</p><h2 class="nickname">rayo88 </h2><b class="age">23v.</b> Tampere</div>';

//Random number for random story
var array = [content1, content2, content3, content4, content5, content6];
var randomNumber1 = Math.floor((Math.random() * 2)); //returns 0 or 1
var randomNumber2 = Math.floor((Math.random() * 2 + 1) + 1); //returns 2 or 3
var randomNumber3 = Math.floor((Math.random() * 2 + 2) + 2); //returns 4 or 5

//Append random testimonials to page
document.getElementsByClassName("container-fluid")[4].innerHTML = "<div id='testimonials'>"+title+array[randomNumber1]+array[randomNumber2]+array[randomNumber3]+"</div>";

//Hide quickSearch element (footer)
document.getElementById("quickSearch").style.display = "none";

//Footer icon attribution
document.getElementsByClassName('aller-footer')[0].outerHTML += '<div id="iconCredit"><a id="creditLink" href="https://flaticon.com" target="_blank">Icons designed by Roundicons from Flaticon</a></div>';

//Styles
var testimonials = document.getElementById("testimonials");
var personBoxes = document.getElementsByClassName("person");
var faceImage = document.getElementsByClassName('faceImg');
var quote = document.getElementsByClassName('quote');
var nickname = document.getElementsByClassName('nickname');
var personAge = document.getElementsByClassName('age');
var credits = document.getElementById("iconCredit");
var creditLink = document.getElementById("creditLink");

testimonials.style.margin = "0% auto 5% auto";
testimonials.style.textAlign = "center";
testimonials.style.maxWidth = "1140px";

credits.style.margin = "1% 0";
credits.style.textAlign = "center";
credits.style.fontSize = ".75em";
creditLink.style.color = "#dbdbdb";

for (i = 0; i < personBoxes.length; i++ ) {
  personBoxes[i].style.margin = "2% 1%";
  personBoxes[i].style.textAlign = "center";
  personBoxes[i].style.float = "none";
  personBoxes[i].style.display = "inline-block";
  faceImage[i].style.width = "25%";
  personAge[i].style.display = "block";
  personAge[i].style.marginTop = "-7.5px";
  personAge[i].style.fontSize = "1.25em";
  quote[i].style.fontSize = "1.5em";
  quote[i].style.margin = "20px 0 0 0";
  quote[i].style.padding = "0 2.5%";
  quote[i].style.lineHeight = "1.33em";
  nickname[i].style.fontSize = "1.4em";
}
