/*
Domain: Treffit.suomi24.fi
Experiment name: Treffit Send KD analytics event on element click
Version: -
Platform: -
Author: Thomas
*/

//Selector
var element = document.querySelectorAll('h2')[0];

//Click event to selected element
element.addEventListener("click", function(){
  //Send GA event
  kd.analytics.send('event', 'Experiments', 'NewFrontPage', 'Sign up button clicked');
});
