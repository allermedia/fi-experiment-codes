/*
Domain: Treffit.suomi24.fi
Experiment name: Mobile navigation sign in UX improvement
Version: 1.0
Platform: VWO
Author: Thomas
*/

//Define mobile hamburger button
var hamburgerNav = document.getElementsByClassName('navbar-toggle')[0];

//Show sign up inputs by default
hamburgerNav.addEventListener('click', function(){
  //Force click on sign up link
  var signUpLink = document.querySelectorAll('#treffit-navbar-collapse ul li a')[1];
  signUpLink.click();
});
