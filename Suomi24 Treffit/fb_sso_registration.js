/*
Domain: Treffit.suomi24.fi
Experiment name: Treffit Facebook SSO registration validation
Version: 1.0
Platform: Optimizely
Author: Thomas
*/

//Add Facebook button to registration form
document.querySelectorAll('.form-group .btn-primary')[1].outerHTML += '<div id="SSO"><br/><b id="boldStyles">... tai</b><div id="SSOFB">Kirjaudu Facebookilla</div></div>';
document.querySelectorAll('.form-group .btn-primary')[1].outerHTML += '<div id="disclaimerSSO"></div>';

//Styles to Facebook button
var FbCTAStyles = "background: #4267b2; color: #FFF; padding: 2% 5%; text-align:center; border-radius: 30px; cursor: pointer; font-size: 1.25em; text-transform: uppercase; margin: 2% 0; width: 70%;";
document.getElementById('SSOFB').style = FbCTAStyles;
var boldStyles = "font-size: 1.33em;"
document.getElementById('boldStyles').style = boldStyles;

//Add click event to Facebook SSO button
document.getElementById("SSOFB").addEventListener("click", function(){
    document.getElementById("disclaimerSSO").innerHTML = '<p id="disclaimerSSO"><br/>Valitettavasti tämä on vielä työn alla.<br/><b>Voit kuitenkin rekiströityä yllä olevalla lomakkeella.</b></p>';
    document.getElementById("disclaimerSSO").style.fontSize = '1.33em';
    document.getElementById("SSO").style.display = 'none';
});
