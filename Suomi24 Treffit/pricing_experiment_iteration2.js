/*
Domain: Treffit.suomi24.fi
Experiment name: Treffit pricing experiment iteration
Version: 2.0
Platform: Optimizely
Author: Thomas
*/

//Add header content
$('.plus-promo .container-fluid:eq(0)').prepend('<h2 class="headLine">Enemmän näkyvyyttä ja viestittele vaikka sadalla päivässä.</h2>');
//Headline styles
$('.headLine').css('text-align','center');

//Re-arrange subscription products
$('#continuous_subscription').insertBefore('#two_week_subscription');
$('#one_month_subscription').insertBefore('#two_week_subscription');
$('#three_month_subscription').insertBefore('#two_week_subscription');

//Show 3 month product
$('#three_month_subscription').removeClass('hidden');

//Hide three month subscription ribbon
$('#three_month_subscription .ribbon').remove();

//Continuous ribbon text
$('#continuous_subscription .ribbon').text('Suosittelemme');

//Continuous product
$('#continuous_subscription h1').text('Jatkuva tilaus');

//Style continuous box ribbon
$('#continuous_subscription .ribbon').css({
    'margin' : '-25px 0 20px 0',
    'padding' : '3% 0%',
    'position' : 'relative',
    'width' : '100%'
});

//Subscription texts contents
$('#continuous_subscription .subscription-text').html("<p>Huoleton valinta.<br/>Saat <b><u>4 kpl</u></b> megafonihuutoa joka kuukausi.</p>");
$('#one_month_subscription .subscription-text').html("<p>Kestosuosikki.<br/>Saat <b><u>4 kpl</u></b> megafonihuutoa.</p>");
$('#three_month_subscription .subscription-text').html("<p>Tehopakkaus.<br/>Saat <b><u>12 kpl</u></b> megafonihuutoa.</p>");
$('#two_week_subscription .subscription-text').html("<p>Kokeile nopeasti.<br/>Saat <b><u>2 kpl</u></b> megafonihuutoa.</p>");

//Styles for subscription texts
$('.subscription-text b u').css({
    'color' : '#000',
    'font-weight' : 'bold'
});

$('.subscription-text').css('padding','0');

//Monthly prices
$('#threemonths_orig_price').append('<span class="monthly_price">(9,99€/kk)</span>');
$('#twoweeks_orig_price').append('<span class="monthly_price">(19,90€/kk)</span>');

$('.monthly_price').css({
    'color' : '#c0c0c0',
    'font-size' : '.6em',
    'font-weight' : '100'
});

//Subscription price style fixes
$('.subscription-price-euro').css('margin','0 -5px 0 0');
$('.subscription-buttons').css('padding-top','10px');

//Product header
$('.product-wrapper-inner h1').css({
  'position' : 'relative',
  'margin-top ': '0'
});

//Product borders
$('.kd-subscription-container').css('border' , '3.5px solid #98c04d');

//Agreement info styles (continuous product)
$('#agreement-info').css('margin','2% 0');

if ( window.innerWidth > 768 ) {
    $('h2.headLine').css('margin','-10px 0 30px 0');
    $('#continuous_subscription h1').html('<h1>Jatkuva<br/> tilaus</h1>');
}


if ( window.innerWidth < 768 ) {
  //Fix mobile bottom part
  $('.feature_box').css('width', '90%');
  $('#UVP').css('width','90%');
}
