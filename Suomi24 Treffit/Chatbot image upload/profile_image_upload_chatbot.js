/*
Domain: Treffit.suomi24.fi
Experiment name: Profile image upload chatbot
Version: 1.0
Platform:
Author: Mattia, Ruchi, Thomas
*/


function setCookie(name, value) {
  //Set cookie valid for a month
  var d = new Date();
  d.setTime(d.getTime() + (30*24*60*60*1000));
  var expires = 'expires=' + d.toUTCString();
  document.cookie = name+'='+value+';'+ expires + ';path=/';
}
function readCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) === 0) return c.substring(nameEQ.length, c.length);
  }
  return null;
}
function showChatBot(name, imageSrc) {
  function addMessage(content, dotsDuration, onComplete) {
    var dots = $(
      '<div>'+
        '<img src='+imageSrc+'>'+
        '<div class="chat-message-container">'+
          '<div class="chat-message-content">'+
            '<div id="wave">'+
                name+' kirjoittaa '+
                '<span class="dot"></span>'+
                '<span class="dot"></span>'+
                '<span class="dot"></span>'+
            '</div>'+
        '</div>'+
      '</div>'
    );
    $('.chat-body').append(dots);
    setTimeout(function() {
      dots.remove();
      $('.chat-body').append(
        '<div>'+
          '<img src='+imageSrc+'>'+
          '<div class="chat-message-container element-animation1">'+
            '<div class="chat-message-content">'+content+'</div>'+
          '</div>'+
        '</div>'
      );
      onComplete();
    }, dotsDuration);
  }

  kd.analytics.send('event', 'Experiments', 'PictureChatbot', 'Chatbot shown');
  $('body').append(
    '<div id="chatbot" class="element-animation">'+
      '<div class="chat-container">'+
        '<div class="chat-header with-padding">'+
          '<i class="icon online-status online"></i>'+
          '<span class="chat-botname">'+name+'</span>'+
          '<div id="close">X</div>'+
        '</div>'+
        '<div class="chat-body with-padding"></div>'+
      '</div>'+
    '</div>'
  );
  // Let the DOM update before adding the listener
  setTimeout(function() {
    $('#close').click(function() {
      setCookie('closedChat', '1');
      $('#chatbot').hide();
		kd.analytics.send('event', 'Experiments', 'PictureChatbot', 'Closed chatbot ');
    });
  }, 10);
  addMessage('Moikka<span style="color: red;"> &#10083;</span>', 3000, function() {
    setTimeout(function() {
      addMessage('Tiesitkö että <u><b>74%</u></b> treffailijoista, pitää kuvallisia profiileja parempina?', 2000, function() {
        $('.chat-body').append(
          '<a class="btn btn-primary change-profile-picture" href="/profilePicture/changeProfilePictureDialog">'+
              'Lisää profiiliisi kuva'+
            '</a>'+
          '<div class="profile-pic js-profile-picture-change" style="display:none"><img /></div>'
        );
        // Let the DOM update before adding the listener
        setTimeout(function() {
          $('#chatbot a.change-profile-picture').click(function() {
            setCookie('closedChat', '1');
          });
        }, 10);

        $(".change-profile-picture").fancybox({
          onComplete: function() {
            kd.analytics.send('event', 'Experiments', 'PictureChatbot', 'Upload modal shown');
            kd.profile.profilePicture.initChangeProfilePictureDialog();
          }
        });
        $(".js-profile-picture-change").one("DOMSubtreeModified", function() {
          kd.analytics.send('event', 'Experiments', 'PictureChatbot', 'New picture uploaded');
          $('.chat-body').empty();
          addMessage('Kiitos, kuvasi on päivitetty :)', 2000, function() {});
        });
      });
    }, 500);
  });
}
setTimeout(function () {
  if(readCookie('closedChat')) return;
  var userPicture = document.querySelector('li.dropdown a img').src;
  var origin = window.location.origin;
  switch (userPicture) {
    case origin+'/img/profile_default_m.png':
      showChatBot('<b>Terhi</b>, <span>deittiasiantuntija</span>', 'https://s14.postimg.org/vgxqosm41/terhi-bot.jpg');
      break;
    case origin+'/img/profile_default_f.png':
      showChatBot('<b>Juha</b>, <span>deittiasiantuntija</span>', 'https://s14.postimg.org/yb0w27tf5/juha-bot.jpg');
      break;
    case origin+'/img/profile_default_u.png':
      showChatBot('<b>Terhi</b>, <span>deittiasiantuntija</span>', 'https://s14.postimg.org/vgxqosm41/terhi-bot.jpg');
      break;
    default:
      // Already has picture!
      break;
  }
}, 3000);
