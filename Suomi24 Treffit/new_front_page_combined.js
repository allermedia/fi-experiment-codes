//HEADER
//Content of header / splash screen
var splashLogo = '<img class="img-responsive subscription-plus-logo" src="https://treffit.suomi24.fi/img/logos/treffit24.svg" alt="Treffit Plus" style="margin: 2% auto;">';
var splashTitle = '<h1 id="splashHeadline" class="col-xs-12">TÄÄLTÄ LÖYDÄT <span id="counter"><u>72250</u></span> IHMISTÄ!</h1>';
var splashCTA = '<div class="col-xs-12"><a href="/profile/create" class="btn-green splashCTA">Liity ilmaiseksi</a></div>';
var splashLoginText = '<b id="splashLogin">Oletko jo jäsen? <a>Kirjaudu tästä</a></b>';
var splashHeader = '<div id="splashSubHeadline">Treffeissä lähetetään jopa 60 000 viestiä päivittäin ja liittyminen kestää vain pari minuuttia.</div>';
document.getElementById('splash-screen').innerHTML = "<div id='splash-screen'><div id='splashContent'>"+splashLogo+splashTitle+splashHeader+splashCTA+splashLoginText+"</div></div>";
//Styles
var splashScreen = document.getElementById('splash-screen');
var splashHeadline = document.getElementById('splashHeadline');
var splashHeader = document.getElementById('splashSubHeadline');
var splashContent = document.getElementById('splashContent');
var splashCTAButton = document.getElementsByClassName('splashCTA')[0];
var splashLoginCTA = document.getElementById('splashLogin');
splashScreen.style.margin = "0 auto 2% auto";
splashScreen.style.textAlign = "center";
splashScreen.style.maxWidth = "100%";
splashScreen.style.background = "url('https://images.pexels.com/photos/196666/pexels-photo-196666.jpeg') no-repeat";
splashScreen.style.backgroundSize = "cover";
splashScreen.style.backgroundPositionY = "-350px";
splashContent.style= "background-color: rgba(255,255,255,.8); width: 50%; margin: 2% auto; padding: 2% 0;";
splashHeadline.style.margin = "1.5% 0 2% 0";
splashSubHeadline.style = "font-size: 1.5em; width: 50%; margin: auto;";
splashCTAButton.style = "font-size: 1.25em; font-weight: bold; margin: 25px auto; background-color: #98C04D; padding: 20px; color: #fff; border-radius: 4px; text-transform: uppercase; display: block; max-width: 350px;";
splashLoginCTA.style.cursor = "pointer";
if ( window.innerWidth < 1400 ) {
  splashScreen.style.backgroundPositionY = "-200px";
}
if ( window.innerWidth < 1000 ) {
  splashScreen.style.backgroundPositionY = "0px";
  splashContent.style.width = "100%";
}
//Log in click event
document.getElementById('splashLogin').addEventListener("click", function(){
  document.getElementsByTagName('ul')[0].getElementsByTagName('a')[1].click();
  window.scrollTo(0,0);
});
//Counter
var counterElement = document.getElementById('counter');
var counter = 72250;
var j = setInterval(function(){
  counter++;
  if ( counter >= 72285 ) {
    clearInterval(j);
    var k = setInterval(function(){
      counter++;
      counterElement.innerHTML = "<u>"+counter+"</u>";
      if ( counter === 72289 ) {
        clearInterval(k);
      }
    }, 500 );
  }
  counterElement.innerHTML = "<u>"+counter+"</u>";
}, 100 );
//HEADER END
//FOOTER
//Content
var title = '<h1 class="col-xs-12">Treffailijat haluavat jakaa kokemuksiaan kanssasi</h1>';
//Testimonials
var content6 = '<div class="person col-md-3"><img class="faceImg" src="https://s17.postimg.cc/smjhlv2tb/smiling.png"><p class="quote">Löysin sielunkumppanin. Olen onnellinen ja niin on hänkin :D</p><h2 class="nickname">jozzukka </h2><b class="age">46 v.</b> Uusimaa</div>';
var content5 = '<div class="person col-md-3"><img class="faceImg" src="https://s17.postimg.cc/ip8gssnhr/in-love.png"><p class="quote">Yksistä pienistä viattomista treffeistä tuli jotain ihan muuta...</p><h2 class="nickname">yourprincessnanna </h2><b class="age">19 v.</b> Joensuu</div>';
var content4 = '<div class="person col-md-3"><img class="faceImg" src="https://s17.postimg.cc/smjhlv2tb/smiling.png"><p class="quote">Aivan ihanasti löytyi todellinen herrasmies. Elämä on ihanaa!</p><h2 class="nickname">radiorock </h2><b class="age">51 v.</b> Satakunta</div>';
var content3 = '<div class="person col-md-3"><img class="faceImg" src="https://s17.postimg.cc/ip8gssnhr/in-love.png"><p class="quote">Löysin parhaan mahdollisen kumppanin Suomi24 Treffeiltä. :)</p><h2 class="nickname">Ninduliina </h2><b class="age">25 v.</b> Pirkanmaa </div>';
var content2 = '<div class="person col-md-3"><img class="faceImg" src="https://s17.postimg.cc/6alosgo9r/happy-2.png"><p class="quote">Löysin kivan kaverin, jonka kanssa nyt treffailemme.</p><h2 class="nickname">Merkku12 </h2><b class="age">52 v.</b> Pohjois-Savo</div>';
var content1 = '<div class="person col-md-3"><img class="faceImg" src="https://s17.postimg.cc/6alosgo9r/happy-2.png"><p class="quote">Ei menny kun pari päivää kun löysin täältä sopivan kumppanin...</p><h2 class="nickname">rayo88 </h2><b class="age">23 v.</b> Tampere</div>';
//Footer icon attribution
document.getElementsByClassName('aller-footer')[0].outerHTML += '<div id="iconCredit"><a id="creditLink" href="https://flaticon.com" target="_blank">Icons designed by Roundicons from Flaticon</a></div>';
//RNG for random story
var array = [content1, content2, content3, content4, content5, content6];
var randomNumber1 = Math.floor((Math.random() * 2)); //returns 0 or 1
var randomNumber2 = Math.floor((Math.random() * 2 + 1) + 1); //returns 2 or 3
var randomNumber3 = Math.floor((Math.random() * 2 + 2) + 2); //returns 4 or 5
//Hide profiles (change to testimonials)
document.getElementsByClassName("container-fluid")[4].innerHTML = "<div id='testimonials'>"+title+array[randomNumber1]+array[randomNumber2]+array[randomNumber3]+"</div>";
//Hide quickSearch (footer)
document.getElementById("quickSearch").style.display = "none";
//Styles
var testimonials = document.getElementById("testimonials");
var personBoxes = document.getElementsByClassName("person");
var faceImage = document.getElementsByClassName('faceImg');
var quote = document.getElementsByClassName('quote');
var nickname = document.getElementsByClassName('nickname');
var personAge = document.getElementsByClassName('age');
var credits = document.getElementById("iconCredit");
var creditLink = document.getElementById("creditLink");
testimonials.style.margin = "0% auto 5% auto";
testimonials.style.textAlign = "center";
testimonials.style.maxWidth = "1140px";
credits.style.margin = "1% 0";
credits.style.textAlign = "center";
credits.style.fontSize = ".75em";
creditLink.style.color = "#dbdbdb";
for (i = 0; i < personBoxes.length; i++ ) {
  personBoxes[i].style.margin = "2% 1%";
  personBoxes[i].style.textAlign = "center";
  personBoxes[i].style.float = "none";
  personBoxes[i].style.display = "inline-block";
  faceImage[i].style.width = "25%";
  personAge[i].style.display = "block";
  personAge[i].style.marginTop = "-7.5px";
  personAge[i].style.fontSize = "1.25em";
  quote[i].style.fontSize = "1.5em";
  quote[i].style.margin = "20px 0 0 0";
  quote[i].style.padding = "0 2.5%";
  quote[i].style.lineHeight = "1.33em";
  nickname[i].style.fontSize = "1.4em";
}
//FOOTER ITERATION 
//Footer
var footerTitle = '<h2 class="col-xs-12">Haku päällä?</h2>';
var footerParagraph = "<p class='col-xs-12' id='footerParagraph'>Haku siirtyi kirjautumisen taakse ja Treffit on nyt entistä turvallisempi. Rekisteröinti on ilmaista ja nopeaa. Luo tili niin pääset selailemaan profiileja tuota pikaa!</p>";
var footerCTA = '<div class="col-xs-12"><a href="/profile/create" class="btn-green splashCTA">Liity ilmaiseksi</a></div>';
var footerRegLink = '<b id="footerLogin">Oletko jo jäsen? <a>Kirjaudu tästä</a></b>';
//Append content
var footer = document.getElementById('testimonials').innerHTML += "<div id='footer'>"+footerTitle+footerParagraph+footerCTA+footerRegLink+"</div>";
//Styles
var footerContainer = document.getElementById('footer');
var footerCTA = document.getElementsByClassName('splashCTA')[1];
var footerParagraph = document.getElementById('footerParagraph');
var footerRegLinkStyles = document.getElementById('footerLogin');
footerContainer.style= "margin: 2% 0";
footerCTA.style = "font-size: 1.25em; font-weight: bold; margin: 25px auto; background: #98C04D; padding: 20px; color: #fff; border-radius: 4px; text-transform: uppercase; display: block; max-width: 350px;";
footerParagraph.style = "padding: 0 10%; font-size: 1.5em; line-height: 36px;";
footerRegLinkStyles.style = "cursor: pointer";
//Trigger Hotjar poll + recording
hj('trigger', 'quick_win_design');
document.getElementById('footerLogin').addEventListener("click", function(){
  document.getElementsByTagName('ul')[0].getElementsByTagName('a')[1].click();
  window.scrollTo(0,0);
});
