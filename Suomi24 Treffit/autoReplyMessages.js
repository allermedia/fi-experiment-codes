/*
Domain: Treffit.suomi24.fi
Experiment name: Auto-reply messages
Version: 3.0
Platform: VWO
Author: Alice & Thomas
*/

//style answers
var answerStyle =	"style= 'display: inline-block; margin: 10px 5px 0 0; padding: 5px; background: #fff; color: #000; cursor: pointer;' ";
//style headline
var headlineStyle = " style='margin: 10px 0 0 0; font-size: 1.2em; font-weight: bold;'";

//create automatic answers
var autoReplyTitle =	"<div id='auto-reply-title' " +	headlineStyle +	'>Älä jätä henkilöä ilman vastausta:</div>';
var answer1 = '<div class="answer" id="answer1" ' + answerStyle + '>Kiitos, mutta en ole kiinnostunut.</div>';
var answer2 = '<div class="answer" id="answer2" ' + answerStyle + ">Kertoisitko vähän enemmän itsesäsi :)</div>";
var answer3 = '<div class="answer" id="answer3" ' + answerStyle + ">Kiitos viestistä, palaan sinulle vähän myöhemmin.</div>";
var feedbackLink = '<br/><br/><span>Puuttuuko jokin vastaus?</span><br/><a href="https://in.hotjar.com/s?siteId=716829&surveyId=90251" target="_blank">Anna palautetta</a>';

//show answers
var sendButton = document.querySelector('.message-form-textarea-container');

var content = document.createElement('div');
content.id = 'experiment-container';
content.innerHTML = autoReplyTitle + answer1 + answer2 + answer3 + feedbackLink;
sendButton.appendChild(content);

//Loop through all answer buttons
for (var i = 0; i < 3; i++) {
	//Variable for answer buttons
	var answerButtons = document.querySelectorAll('.answer');
	//click answers and magic happens
	answerButtons[i].addEventListener('click', function() {
		//do stuff
		document.querySelector('.kd-flash-message-container textarea').value = this.innerText;
		//send
    document.querySelector('.messages-form-send-button button.btn').click();
    //hide answer buttons when answered
    document.querySelector('#experiment-container').style.display = 'none';
		//send custom GA event
		kd.analytics.send('event', 'Messaging', 'auto-reply-sent', 'Experiment');
	});
}
