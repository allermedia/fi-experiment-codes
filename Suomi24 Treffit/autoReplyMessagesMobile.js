/*
Domain: Treffit.suomi24.fi
Experiment name: Auto-reply messages (mobile optimized horizontal scroll)
Version: 4.0
Platform: VWO
Author: Alice & Thomas
*/

//Define conversation threads
var conversationThreads = document.getElementsByClassName('conversation');

//Onclick event for all conversation threads in mobile
for (var a = 0; a < conversationThreads.length; a++) {
	conversationThreads[a].addEventListener('click', function() {
		setTimeout(function(){
			//style answers
			var answerStyle = " style= 'display: inline; margin: 10px 5px 0 5px; padding: 10px 0 7.5px 0; border-radius: 4px; text-align: center; background: #fff; color: #000; cursor: pointer; min-width: 250px;' ";
			//emojiStyles
			var emojiStyles = " style='max-width: 25px;' ";
			//automatic answer content
			var answer1 = '<div class="answer" id="answer1" ' + answerStyle + '>Kiitos, mutta en ole kiinnostunut.</div>';
			var answer2 = '<div class="answer" id="answer2" ' + answerStyle + '>Kertoisitko vähän enemmän itsestäsi :)</div>';
			var answer3 = '<div class="answer" id="answer3" ' + answerStyle + '>Palaan sinulle vähän myöhemmin.</div>';
			var answer4 = '<div class="answer" id="answer4" ' + answerStyle + '>Miten päiväsi on kulunut?</div>';
			var answer5 = '<div class="answer" id="answer5" ' + answerStyle + '>Lähettäisitkö minulle kuvasi, kiitos :)</div>';
			//emojies
			var emoji1 = "<div class='answer emoji' id='emoji1'" + answerStyle + "id='answer6'><img" + emojiStyles + " src='https://s17.postimg.cc/smjhlv2tb/smiling.png'>:)</div>";
			var emoji2 = "<div class='answer emoji' id='emoji2'" + answerStyle + "id='answer7'><img" + emojiStyles + " src='https://s17.postimg.cc/ip8gssnhr/in-love.png'><3</div>";
			var emoji3 = "<div class='answer emoji' id='emoji3'" + answerStyle + "id='answer8'><img" + emojiStyles + " src='https://s17.postimg.cc/6alosgo9r/happy-2.png'>:D</div>";
			//show answers (jquery)
			var messagesFooter = document.querySelector('.messages-room-footer');
			$(messagesFooter).prepend("<div id='experiment-container'>" + emoji1 + emoji2 + emoji3 + answer1 + answer2 + answer3 + answer4 + answer5 + "</div>");
			//Smooth horizontal scroll on auto-reply box
			var experimentContainer = document.querySelector('#experiment-container');
			experimentContainer.style = "display: flex; overflow: scroll; -webkit-overflow-scrolling: touch; margin: -10px 0 5px 0; padding: 0 0 10px 0;";
			//Loop through emoji containers
			var emoji = document.querySelectorAll('.emoji');
			for (var k = 0; k < 3; k++) {
				emoji[k].style.minWidth = "50px";
				emoji[k].style.color = "#FFF";
				emoji[k].style.fontSize = "0";
			}
			//Make chat area smaller
			document.querySelector('.messages-room-content').style.height = "80%";
			//Loop through all answer buttons
			for (var i = 0; i < 7; i++) {
				//Variable for answer buttons
				var answerButtons = document.querySelectorAll('.answer');
				//click answers and magic happens
				answerButtons[i].addEventListener('click', function() {
					//do stuff
					document.querySelector('.kd-flash-message-container textarea').value = this.innerText;
					//send
					document.querySelector('.messages-form-send-button button.btn').click();
					//hide answer buttons when answered
					document.querySelector('#experiment-container').style.display = 'none';
					//send custom GA event
					kd.analytics.send('event', 'Messaging', 'auto-reply-sent', 'Experiment');
				});
			}
		}, 1000);
	});
}
