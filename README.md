# Experiment Codes

This repository contains reusable A/B testing and experiment code that can be utilised in tools such as VWO and Optimizely.

##### Coding policy
- Use explanatory file names (e.g. newsletter_popup_variation1 instead of variation1)
- Use comments in code to make it easier to read and understand
- Tabs not spaces ;)
- Ask a developer to review the code before publishing the experiment

##### Installation
Clone the repository:

```sh
cd /the/location/of/your/choice
git clone https://thomasdjupsjo@bitbucket.org/allermedia/fi-experiment-codes.git
```

##### Updates
To get the newest updates use the following:
```sh
cd /the/location/of/fi-experiment-codes/folder
git pull
```
##### License
Aller Media Oy
