/*
Domain: Seiska.fi
Experiment name: Seiska Mobile site add to homescreen experiment
Version: 1.0
Platform: Optimizely
Author: Thomas
*/

//Add to home screen promo

//If device is wider than 345px
if ( window.innerWidth > 345 ) {

  //Hide anniversary campaign box
  $('#orderBox #close').click();

  //Add box and contents
  $('body').append('<div id="homeScreen"></div>');
  $('#homeScreen').append('<div class="noThx">&#10005;</div>');
  $('#homeScreen').append('<div id="appLogo"><img class="webAppLogo" src="/themes/custom/seiska2016/logo_normal.svg"></div>');
  $('#homeScreen').append('<div id="homeScreenCTA"><span class="btn btn-primary btn-lg">Lisää Seiska aloitusnäytölle</span></div>');

  //Styles
  $('#homeScreen').css({
    'position': 'fixed',
    'bottom' : '0',
    'background' : '#fff',
    'padding' : '2% 3%',
    'width' : '100%',
    'z-index' : '2',
    'font-size' : '1.1em',
    'border-top' : '7.5px solid #ffe300'
  });

  $('.noThx').css({
    'font-size' : '1.5em',
    'float' : 'right',
    'position' : 'relative',
    'top' : '18px'
  });

  $('#homeScreenCTA').css({
    'display' : 'inline-block',
    'position' : 'relative',
    'top' : '12.5px'
  });

  $('#appLogo').css({
    'text-align' : 'center',
    'width' : '60px',
    'height' : '60px',
    'background' : '#000',
    'border-radius' : '15px',
    'display' : 'inline-block',
    'margin' : '0 2% 0 0'
  });

  $('img.webAppLogo').css({
    'width' : '30px',
    'position' : 'relative',
    'top' : '12px',
    'left' : '3px'
  });

  //CTA button onclick event
  $('#homeScreen .btn').click(function(){
      $('#appLogo').remove();
      $('#homeScreenCTA').html('<b>Kiitos mielenkiinnosta!</b><br/>Puhelimesi valikosta valitse <i>"Lisää aloitusnäytölle."</i><br/><br/></p>');
      var d = new Date();
      d.setTime(d.getTime() + (30*24*60*60*1000));
      var expires = "expires="+ d.toUTCString();
      document.cookie = "homeScreenBox=1;"+ expires + ";path=/";
  });

  //Close button onclick event
  $('#homeScreen .noThx').click(function(){
    $('#homeScreen').remove();
    var d = new Date();
    d.setTime(d.getTime() + (30*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = "homeScreenBox=1;"+ expires + ";path=/";
  });

}
