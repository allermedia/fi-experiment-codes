/*
Domain: Seiska.fi
Experiment name: Order Seiska fixed campaign box experiment
Version: 1.0
Platform: Optimizely
Author: Thomas
*/

//Add box content
document.querySelector('body').innerHTML += '<div id="orderBox"><h3>Seiska tekee arjesta hauskempaa!</h3><p>Tilaa Seiska nyt ja säästä jopa <u><b>50%</b></u></p><div id="orderSeiska">Tilaa Seiska</div><div id="close">&#10005;</div></div>';

//Styles for box element
var orderBoxStyles = "visibility: hidden; position: fixed; bottom: 5px; background: #fff; padding: 10px 10px 10px 20px; z-index: 2; border-top: 10px solid #00ADEE; border-right: 2px solid #b9b9b9; border-left: 2px solid #b9b9b9; border-bottom: 2px solid #b9b9b9; box-shadow: 2px 2px 10px rgba(0,0,0,.25); border-radius: 2px; font-size: 1.1em;";
document.querySelector('#orderBox').style = orderBoxStyles;

var orderBoxH3Styles = "display: inline; font-size: 20px;";
document.querySelector('#orderBox h3').style = orderBoxH3Styles;

var orderBoxParagraphStyles = "display: inline; margin: 0 20px 0 10px";
document.querySelector('#orderBox p').style = orderBoxParagraphStyles;

var orderBoxOfferStyles = "color: green;";
document.querySelector('#orderBox u').style = orderBoxOfferStyles;

var orderBoxButtonStyles = "display: inline; background: #ec1d24; color: #fff; border: 1px solid #ec1d24; padding: 5px 15px; margin: 0 10px 0 0; cursor:pointer; text-transform: uppercase; font-weight: bold; font-family: 'Roboto Condensed', sans-serif;";
document.querySelector('#orderBox #orderSeiska').style = orderBoxButtonStyles;

var orderBoxNoThanksStyles = "display: inline; margin: 0 5px; font-size: 1.2em; cursor: pointer; text-align: center;";
document.querySelector('#orderBox #close').style = orderBoxNoThanksStyles;

//Click event listener for campaign box close button
document.getElementById("close").addEventListener("click", function(){
  //Hide box
  document.querySelector('#orderBox').style.display = "none";
  //Set cookie valid for a month
  var d = new Date();
  d.setTime(d.getTime() + (30*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = "orderBox=1;"+ expires + ";path=/";
});

//Click event listener for campaign box CTA button
document.getElementById("orderSeiska").addEventListener("click", function(){
  //Set cookie valid for a month
  var d = new Date();
  d.setTime(d.getTime() + (30*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = "orderBox=1;"+ expires + ";path=/";
  //Redirect user to Seiska offer page
	window.location.href = "https://aller.soihtu.eu/3847/245/tarjous/seiska?utm_source=seiska_article&utm_medium=experiment&utm_campaign=tilaa_seiska";
});

//Show box on scroll +800px
window.onscroll = function() {scrollFunction()};

function scrollFunction(){
    if ( window.scrollY > 800 ) {
			document.getElementById("orderBox").style.visibility = "visible";
      //Center box
			document.querySelector("#orderBox").style.left = (window.innerWidth-document.querySelector("#orderBox").clientWidth)*0.5+"px";
		}
}
