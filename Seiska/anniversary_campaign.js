/*
Domain: Seiska.fi
Experiment name: Spring campaign box code
Version: 1.0
Platform: GTM
Author: Thomas
*/

<style>
/* Hide image in mobile*/
@media only screen and (max-width: 1300px) {
    #campaignImage {
    	display:none;
    }
    #orderSeiska {
      display: block !important;
      margin: 20px auto 10px !important;
      max-width: 200px;
    }
}
/* Mobile styles */
@media only screen and (max-width: 768px) {
  	#orderSeiska {
	  	display: block !Important;
    	margin: 10px auto !Important;
      padding: 15px 0 !important;
  	}
    h3 {
      font-size: 30px !important;
      display: block !important;
    }

    #timer div {
      display: block;
    }

    #orderBox p {
      display: none !important;
    }
    #orderBox #close span {
      display: none !important;
    }

}

#timer {
    font-size: 1.75em;
    margin: .5% 0;
}

#timer div {
    display: inline;
}

div#timer u {
    color: red;
    font-weight: bold;
}
</style>
<script type="text/javascript">
if ( document.cookie.indexOf('orderBox') == -1 ) {

  //Add box content
  document.querySelector('body').innerHTML += '<img id="campaignImage" src="https://s17.postimg.org/intijpxr3/campaignImage_DSC_5640.png"><div id="orderBox"><div id="close">&#10005;<span> Sulje</span></div><div id="timer"></div><p>Tilaa Seiska ja saat valitsemasi lahjan kaupan päälle, lahjan arvo jopa 89€!</p><div id="orderSeiska">Tilaa Seiska</div></div>';

  //Styles for box element
  var orderBoxStyles = "width: 100%; text-align: center; position: fixed; bottom: 0px; background: #fff; padding: 10px 10px 20px 20px; z-index: 3; border-top: 10px solid #00ADEE; border-right: 2px solid #b9b9b9; border-left: 2px solid #b9b9b9; border-bottom: 2px solid #b9b9b9; box-shadow: 2px 2px 10px rgba(0,0,0,.25); border-radius: 2px; font-size: 1.1em;";
  document.querySelector('#orderBox').style = orderBoxStyles;

  var orderBoxImgStyles = "width: 180px; position: fixed; bottom: -30px; margin-left: 50%; transform: rotate(7deg); z-index: 2;";
  document.querySelector('img#campaignImage').style = orderBoxImgStyles;

  var orderBoxParagraphStyles = "display: inline; margin: 0 20px 0 10px; font-size: 1.1em;";
  document.querySelector('#orderBox p').style = orderBoxParagraphStyles;

  var orderBoxButtonStyles = "display: inline; background: #ec1d24; color: #fff; border: 1px solid #ec1d24; padding: 5px 15px; margin: 0 10px 0 0; cursor:pointer; text-transform: uppercase; font-weight: bold; font-family: 'Roboto Condensed', sans-serif;";
  document.querySelector('#orderBox #orderSeiska').style = orderBoxButtonStyles;

  var orderBoxNoThanksStyles = "margin: 0 5px; font-size: 1.2em; cursor: pointer; float: right;";
  document.querySelector('#orderBox #close').style = orderBoxNoThanksStyles;

  //Click event listener for campaign box close button
  document.getElementById("close").addEventListener("click", function(){
    //Hide box
    document.querySelector('#orderBox').style.display = "none";
    document.querySelector('#campaignImage').style.display = "none";
    //Set cookie valid for a month
    var d = new Date();
    d.setTime(d.getTime() + (30*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = "orderBox=1;"+ expires + ";path=/";
  });

  //Click event listener for campaign box close button
  document.getElementById("orderSeiska").addEventListener("click", function(){
    //Set cookie valid for a month
    var d = new Date();
    d.setTime(d.getTime() + (30*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = "orderBox=1;"+ expires + ";path=/";
    //Redirect user to Seiska offer page
  	window.location.href = "https://aller.soihtu.eu/3890/245/tarjous/seiska?utm_source=seiska_fi&utm_medium=tagmanager_boksi&utm_campaign=synttaritarjous2018";
  });


  //TIMER TO CAMPAIGN BOX

  // Set the date we're counting down to
  var countDownDate = new Date("Apr 30, 2018 00:00:00").getTime();

  // Update the count down every 1 second
  var x = setInterval(function() {

    // Get todays date and time
    var now = new Date().getTime();

    // Find the distance between now an the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Display the result in the element with id="demo"
    document.getElementById("timer").innerHTML = "<div><h3>HUIKEA SYNTTÄRITARJOUS: </h3><u>" + days + "pv " + hours + "h "
    + minutes + "min " + seconds + "sek </u></div>";

    var orderBoxH3Styles = "display: inline; font-size: 30px;";
    document.querySelector('#timer h3').style = orderBoxH3Styles;

    // If the count down is finished, write some text
    if (distance < 0) {
      clearInterval(x);
      document.getElementById("timer").innerHTML = "EXPIRED";
    }
  }, 1000);

}
</script>
