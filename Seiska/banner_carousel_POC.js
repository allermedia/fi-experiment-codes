/*
Domain: Seiska.fi
Experiment name: Seiska front page banner carousel POC
Version: 1.0
Platform: VWO
Author: Thomas
*/

//Define banner slot container
var bannerContent = document.querySelector('.ad-inhouse-desktop');
//Array of banner variation contents
var bannerContents = ['<a href="#" data-analytics-click="banner-desktop"><img class="img-responsive" src="https://s22.postimg.cc/8wy6403ip/banneri1.png"></a>','<a href="#" data-analytics-click="banner-desktop"><img class="img-responsive" src="https://s22.postimg.cc/8k6rxtsyp/banneri2.png"></a>'];

//Carousel
setInterval(function() {
  //Set amount of banner variants in IF statement
  if ( i < 2 ) {
    bannerContent.innerHTML = bannerContents[i];
    i++;
  }
  //Reset to default banner and loop
  else {
    bannerContent.innerHTML = '<a href="https://aller.soihtu.eu/4038/245/tarjous/seiska" data-analytics-click="banner-desktop"><img class="img-responsive" src="https://fi-seiska-cdn-pro.seiska.fi/files/inhouse_1_desktop.png?cb=1536308789"></a>';
    i = 0;
  }
  //Set banner rotation frequency
}, 5000);
