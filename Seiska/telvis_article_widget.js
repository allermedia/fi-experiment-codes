/*
Domain: Seiska.fi
Experiment name: Telvis banner in articles
Version: 1.0
Platform: Optimizely
Author: Thomas
*/

//Banner content
jQuery('<div id="telvisBanner"></div>').insertAfter('.article-body');
jQuery('#telvisBanner').append('<img id="telvisBanner--image" src="https://www.telvis.fi/static/img/telvis-logo.png">');
jQuery('#telvisBanner').append('<b>Tämänkin ohjelman esitysajat löydät Telviksestä</b><a href="https://telvis.fi">Katso ohjelmatiedot</a>');

//Styles
jQuery('#telvisBanner').css({
  'background' : '#000',
  'color' : '#fff',
  'padding' : '5% 7.5%',
  'margin' : '5% 0'
});

jQuery('#telvisBanner--image').css({
'margin' : '1% auto 5%',
'display' : 'block'
});

jQuery('#telvisBanner a').css({
  'background' : '#EC1D24',
  'display' : 'block',
  'width' : '250px',
  'border-radius' : '30px',
  'padding' : '2.5% 5%',
  'text-align' : 'center',
  'color' : '#fff',
  'margin' : '5% auto'
});

jQuery('#telvisBanner b').css({
  'text-align' : 'center',
  'width' : '100%',
  'display' : 'block',
  'font-size' : '1.25em',
  'font-weight' : '400'
});

//Desktop styles
if ( window.innerWidth > 768 ) {
  jQuery('#telvisBanner').css({
    'padding' : '2.5% 5% 2% 5%'
  });
}
