/*
Domain: Seiska.fi
Experiment name: Seiska klubi banner optimization
Version: 1.0
Platform: VWO
Author: Thomas
*/

//Variation 1 (Red version)
//Change background
document.getElementsByClassName('klubi-banner')[0].style.backgroundColor = "#EC1D24";
//Change border color
document.getElementsByClassName('klubi-banner')[0].style.borderColor = "transparent";

//Variation 2 (Casino)
document.getElementsByClassName('klubi-banner')[0].style.background = "url('https://i.postimg.cc/sfKsBr6C/bg_casino.png')";
document.getElementsByClassName('klubi-banner')[0].style.borderImage = "url('https://i.postimg.cc/65JZjCVh/border_kehys.png')";
document.getElementsByClassName('klubi-banner')[0].style.border = "7.5px dotted #ffb133";

//Variation 3 (sunbeam)
//Set background
document.getElementsByClassName('klubi-banner')[0].style.background = "url('https://static.soihtu.eu/tilaus/css/seiska/180605-bg.jpg') 100%";
document.getElementsByClassName('klubi-banner')[0].style.backgroundSize = "cover";
//Set banner text color
document.getElementsByClassName('klubi-banner__flex')[0].style.color = "#000";
//Remove banner border
document.getElementsByClassName('klubi-banner')[0].style.borderColor = "#000";

//Variation 4
//Left content
document.getElementsByClassName('klubi-banner__left')[0].innerHTML = '<div class="klubi-banner__left"><div class="klubi-banner__text klubi-banner__text--bigger">Liity Seiska-klubiin</div><span>Uusimmat Kääk-juorut yksinoikeudella vain klubin jäsenille! Osallistu ekstrakilpailuihin ja tilaa mahtavia fanituotteita.</span></div>';
//Content middle
document.getElementsByClassName('klubi-banner__center')[0].innerHTML = "<div class='products'><img src='https://i.postimg.cc/nhDF5pgm/seiska_papers.png'/><p>Tilaa ilmainen näytelehti</p></div><div class='products'><img src='https://i.postimg.cc/3J6rndS7/blur_kaak.png'/><p>Lue uusimmat Kääk-juorut</p></div>";
//Styles for variation 4
document.getElementsByClassName('klubi-banner__left')[1].style = "width: 130%;";
document.getElementsByClassName('products')[0].style = "width: 35%; display: inline-block; margin: 0 0 0 20%;";
document.getElementsByClassName('products')[1].style = "width: 35%; display: inline-block; margin: 0 0 0 10%;";
document.querySelectorAll('.products p')[0].style = "margin: 10px 0 0 0; text-transform: uppercase; font-size: .9em; font-weight: bold;";
document.querySelectorAll('.products p')[1].style = "margin: 10px 0 0 0; text-transform: uppercase; font-size: .9em; font-weight: bold;";
document.querySelectorAll('.products img')[0].style = "margin: auto; display: block; position: relative; left: -10px;";
document.querySelectorAll('.products img')[1].style = "margin: auto; display: block; position: relative; left: -10px;";

//Mobile custom variation 4
if ( window.innerWidth < 780 ) {
  //Content
  document.getElementsByClassName('klubi-banner__left')[0].innerHTML = '<div class="klubi-banner__left"><div class="klubi-banner__text klubi-banner__text--bigger">Liity Seiska-klubiin</div></div>';
  document.getElementsByClassName('klubi-banner__center')[0].innerHTML = '<div class="klubi-banner__center"><div class="klubi-banner__text">Uusimmat Kääk-juorut yksinoikeudella vain klubin jäsenille! Osallistu ekstrakilpailuihin ja tilaa mahtavia fanituotteita.</div></div>';
  //Styles
  document.getElementsByClassName('klubi-banner__left')[1].style = "width: 100%;";
}
