/*
Domain: Seiska.fi
Experiment name: Move Suosituimmat section to top of page
Version: 1.0
Platform: Optimizely
Author: Thomas
*/

//Add class to suosituimmat section
$('.row').eq(2).addClass('suosituimmat');
//Place suosituimmat first on page
$('.suosituimmat').prependTo('.row:eq(1)');
//CSS fix for suosituimmat
$('.suosituimmat').css('margin','0');
