/*
Domain: Seiska.fi
Experiment name: Read more links to section boxes on Seiska front page
Version: 1.0
Platform: Optimizely
Author: Thomas
*/

//Define colour variables (for sections)
var yellow = "#FFE300";
var lightBlue = "#00ADEE";
var red = "#EC1D24";
var pink = "#FF0092";
var black = "#000000";

//Add buttons after sections
jQuery('<a class="readMore" href="https://www.seiska.fi/uusimmat/Uutiset">Lue kaikki uutiset</a>').insertAfter('div[data-analytics-scroll="latest"]');
jQuery('<a class="readMore" href="https://www.seiska.fi/Telkkari">Lue kaikki Telkkari-jutut</a>').insertAfter('div[data-analytics-scroll="telkkari"]');
jQuery('<a class="readMore" href="https://www.seiska.fi/Oho">Lue kaikki Oho-jutut</a>').insertAfter('div[data-analytics-scroll="oho"]');
jQuery('<a class="readMore" href="https://www.seiska.fi/Hllwd">Lue kaikki Hllwd-jutut</a>').insertAfter('div[data-analytics-scroll="hllwd"]');
jQuery('<a class="readMore" href="https://www.seiska.fi/HotNow">Lue kaikki HotNow-jutut</a>').insertAfter('div[data-analytics-scroll="hotnow"]');

//Styles
jQuery('a.readMore ').css({
  'display' : 'block',
  'margin' : '0 auto 2.5%',
  'padding' : '20px 0',
  'text-transform' : 'uppercase',
  'text-align' : 'center',
  'text-decoration' : 'underline',
  'cursor' : 'pointer',
  'color' : '#fff',
  'font-size' : '16px',
  'font-weight' : 'bold',
});

jQuery('article').css({
  'overflow' : 'hidden'
});

jQuery('.category-container').css({
  'border-right' : '5px solid',
  'border-left' : '5px solid'
});

jQuery('.category-container div[class*="listing"]').css({
  'margin-left' : '-16px',
  'margin-right' : '-16px'
});

jQuery('div[data-analytics-scroll="latest"]').next().css('background', yellow);
jQuery('div[data-analytics-scroll="latest"]').next().css('color', black);
jQuery('div[data-analytics-scroll="latest"]').css('border-color', yellow);

jQuery('div[data-analytics-scroll="telkkari"]').next().css('background', lightBlue);
jQuery('div[data-analytics-scroll="telkkari"]').css('border-color', lightBlue);

jQuery('div[data-analytics-scroll="oho"]').next().css('background', red);
jQuery('div[data-analytics-scroll="oho"]').css('border-color', red);

jQuery('div[data-analytics-scroll="hllwd"]').next().css('background', pink);
jQuery('div[data-analytics-scroll="hllwd"]').css('border-color', pink);

jQuery('div[data-analytics-scroll="hotnow"]').next().css('background', black);
jQuery('div[data-analytics-scroll="hotnow"]').next().css('color', yellow);
jQuery('div[data-analytics-scroll="hotnow"]').css('border-color', black);
