/*
Domain: Seiska.fi
Experiment name: Add Messenger and Whatsapp links to articles
Version: 1.0
Platform: Optimizely
Author: Thomas
*/

//Move social media button element to article top
jQuery('.somebar').insertAfter('.article-published');

//Delete social media button texts
jQuery('.some-cta').remove();
jQuery('.ctatext').remove();

//Page url variable
var URLToShare = window.location.href;

//Add Headline
document.querySelector('.article-published').outerHTML += '<h4>Jaa juttu sosiaalisessa mediassa:</h4>';

//Add Messenger link
document.querySelector('.somebar a').outerHTML += '<a id="messenger" href="fb-messenger://share/?link='+URLToShare+'"><div class="somelogo vcenter-1 messenger-main"><img src="https://s14.postimg.org/9z14gwjwh/messenger_icon.png"></div></a>';

//Add Whatsapp link
document.querySelector('.somebar a').outerHTML += '<a id="whatsapp" href="whatsapp:\/\/send?text='+URLToShare+'" data-action="share/whatsapp/share"><div class="somelogo vcenter-1 whatsapp-main"><span class="fa fa-whatsapp"></span></div></a>';

//Styles for new buttons
document.querySelector('.whatsapp-main').style.background ="#14A54A";

document.querySelector('.messenger-main').style.background ="#0084ff";
document.querySelector('.messenger-main').style.height ="50px";

document.querySelector('.messenger-main img').style.padding ="5%";
document.querySelector('.messenger-main img').style.width ="35px";
document.querySelector('.messenger-main img').style.position ="relative";
document.querySelector('.messenger-main img').style.top ="7.5px";
document.querySelector('.messenger-main img').style.right ="1px";

//Social media button container styles
jQuery('.somebar').css('margin','10px 0 5px 0');
jQuery('.somelogo').css({
  'width' : '18%',
  'margin' : '0 2px'
});

jQuery('.somebar .fa').css({
  'font-size': '30px'
});
