/*
Domain: Seiska.fi
Experiment name: Seiska search functionality
Version: 1.0
Platform: Optimizely
Author: Thomas
*/

//Append search box
$('.header-right-buttons').append('<input type="text" id="seiska_search" placeholder="Hae sisältöä">');
$('.header-right-buttons').append('<input type="submit" id="seiska_search_button" value="Hae">');

//Search styles
$('input#seiska_search').css({
  'height' : '39px',
  'padding' : '0px 0 0 15px',
  'position' : 'relative',
  'top' : '1px',
  'margin' : '0 0px 0 10px',
  'width' : '270px'
});

$('input#seiska_search_button').css({
  'height' : '38.5px',
  'border' : '0',
  'background' : '#ec1d24',
  'color' : '#fff',
  'font-weight' : 'bold',
  'text-transform' : 'uppercase',
  'width' : '100px'
});

//Search functionalty, click event
$('input#seiska_search_button').click(function(){
  //DEFINE URL's
  var searchQuery = $('input#seiska_search').val();
  var redirectURL = "http://google.fi/search?q="+searchQuery+"&as_sitesearch=seiska.fi";
  window.location.href = redirectURL;
});

//Enter button event trigger
$("input#seiska_search").keyup(function (e) {
  //DEFINE URL's
  var searchQuery = $('input#seiska_search').val();
  var redirectURL = "http://google.fi/search?q="+searchQuery+"&as_sitesearch=seiska.fi";
    if (e.which == 13) {
      window.location.href = redirectURL;
      // ensures the optimizely object is defined globally using
      window['optimizely'] = window['optimizely'] || [];
      // sends a tracking call to Optimizely for the given event name.
      window.optimizely.push(["trackEvent", "enterSearch"]);
    }
});
