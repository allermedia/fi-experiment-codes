/*
Domain: Seiska.fi
Experiment name: Newsletter sign up promotion experiment
Version: 1.0
Platform: Optimizely
Author: Thomas
*/

//Add box content
document.querySelector('body').innerHTML += '<div id="newsLetterBox"><span>Tilaa ilmainen uutiskirje!</span><p>Tilaa Seiskan tuoreimmat uutiset &#151; saat ne kätevästi sähköpostiisi kerran päivässä!</p><button id="newsLetterSignUp">Tilaa uutiskirje</button><div id="close">Ei kiitos</div></div>';

//Styles for sign up box element
var newsLetterBoxStyles = "position: fixed; background: #fff; padding: 0 0 5% 0; z-index: 2; font-size: 1.1em; text-align: center; border-top: 5px solid #ec1d24";
var boxPosition = document.querySelector('#newsLetterBox').clientHeight*(-1);
document.querySelector('#newsLetterBox').style.bottom = boxPosition+"px";
document.querySelector('#newsLetterBox').style = newsLetterBoxStyles;

var newsLetterBoxParagraphStyles = "width: 80%; margin: 0 auto 20px;";
document.querySelector('#newsLetterBox p').style = newsLetterBoxParagraphStyles;

var newsLetterBoxButtonStyles = "background: #ec1d24; color: #fff; border: 1px solid #ec1d24; padding: 2% 5%; margin: 0 10px 0 0; display: inline-block; cursor:pointer;";
document.querySelector('#newsLetterBox button').style = newsLetterBoxButtonStyles;

var newsLetterBoxSpanStyles = "display: block; margin: 0 auto 5%; background: #000; text-align: center; color: #ffe300; text-transform: uppercase; font-weight: bold; padding: 3% 5%; border: 0;";
document.querySelector('#newsLetterBox span').style = newsLetterBoxSpanStyles;

var newsLetterBoxNoThanksStyles = "display: inline; margin: 0 0 0 10px; font-size: .9em;";
document.querySelector('#newsLetterBox #close').style = newsLetterBoxNoThanksStyles;

//Click event listener for newsLetterSignUp button
document.getElementById("newsLetterSignUp").addEventListener("click", function(){
	document.getElementById('newsLetterSignUp').style.display = "none";
  document.querySelector('#newsLetterBox #close').style.display = "none";
	document.querySelector('#newsLetterBox p').innerHTML = "Kiitos mielenkiinnosta. <br/><br/>Valitettavasti uutiskirje on vielä kehityksen alla. Voit kuitenkin jatkaa lukemista Seiskaa osoitteessa <a href='https://seiska.fi'>Seiska.fi</a>";
  document.querySelector('#newsLetterBox a').style.color = "#369";
  //Set cookie valid for a month
  var d = new Date();
  d.setTime(d.getTime() + (30*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = "newsletterBox=1;"+ expires + ";path=/";
});

//Click event listener for newsLetterBox close button
document.getElementById("close").addEventListener("click", function(){
  //Hide box
  document.querySelector('#newsLetterBox').style.display = "none";
  //Set cookie valid for a month
  var d = new Date();
  d.setTime(d.getTime() + (30*24*60*60*1000));
  var expires = "expires="+ d.toUTCString();
  document.cookie = "newsletterBox=1;"+ expires + ";path=/";
});

//Animation
setTimeout(function(){
  function animationFunction() {
    var elem = document.querySelector("#newsLetterBox");
    var pos = 0;
      elem.style.bottom = pos + 'px';
    }
animationFunction();
},2000);
