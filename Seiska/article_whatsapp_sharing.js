/*
Domain: Seiska.fi
Experiment name: Add Whatsapp link to articles
Version: 1.0
Platform: Optimizely
Author: Thomas
*/

//Define headline
var titleToShare = document.querySelector("header h1").innerText;

//Define URL
var URLToShare = window.location.href;

//Add Whatsapp link
document.querySelector('.somebar a').outerHTML += '<a id="whatsapp" href="whatsapp:\/\/send?text='+titleToShare+URLToShare+'" data-action="share/whatsapp/share"><div class="somelogo vcenter-1 whatsapp-main"><span class="fa fa-whatsapp"></span></div><div class="ctatext vcenter-1 whatsapp-light"><span class="some-cta">Jaa <span class="hidden-sm">juttu</span> Whatsapissa</span></div></a>';

//Styles for Whatsapp button
document.querySelector('.whatsapp-main').style.background ="#035a03";
document.querySelector('.whatsapp-light').style.background ="green";
