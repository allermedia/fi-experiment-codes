/*
Domain: Seiska.fi
Experiment name: Seiska article up and down voting
Version: 1.0
Platform: Optimizely
Author: Thomas
*/

//Variable for random value of votes
var randomNumber = Math.floor((Math.random() * 20) + 1);

//Insert content (vote buttons etc.)
jQuery('<div id="votes"></div>').insertBefore('.somebar');
jQuery('#votes').append('<span>Juttu on saanut</span>');
jQuery('#votes').append('<div class="pisteet">'+randomNumber+'</div>');
jQuery('#votes').append('<span class="pisteet_text">ääntä</span><br/>');
jQuery('#votes').append('<div class="arrows"><i class="fa fa-angle-up"></i><br/><span>Tykkäsin</span></div>');
jQuery('#votes').append('<div class="arrows"><i class="fa fa-angle-down"></i><br/><span>En tykännyt</span></div>');

//Click event for up votes
jQuery('.fa-angle-up').one("click", function(){
  //Value of randomNumber +1
  var randomNumberPlusOne = parseInt(jQuery('.pisteet').text())+1;
  jQuery('.pisteet').html(randomNumberPlusOne);
});

//Click event for down votes
jQuery('.fa-angle-down').one ("click", function(){
  //Value of randomNumber -1
  var randomNumberMinuesOne = parseInt(jQuery('.pisteet').text())-1;
  jQuery('.pisteet').html(randomNumberMinuesOne);
});

//If value of votes is 1
if ( parseInt(jQuery('.pisteet').text()) === 1 ) {
  jQuery('.pisteet_text').text('ääni');
}

//Styles
jQuery('#votes').css({
    'border-top' : '5px solid #4DBEA0',
    'padding' : '10px 0px 7.5%',
    'color' : '#000',
    'margin' : '10px 0px 0px -15px',
    'font-size' : '1.15em'
});

jQuery('.pisteet').css({
  'font-size' : '2em',
  'font-weight' : 'bold',
  'display' : 'inline',
  'margin' : '0 1%'
});

jQuery('.arrows').css({
  'text-align': 'center',
  'display': 'inline-block',
  'background' : '#4DBEA0',
  'padding' : '1% 5% 2.5% 5%',
  'margin' : '1% 2% 0 0',
  'color' : '#fff'
});

jQuery('i.fa').css({
  'font-size' : '4em'
});
