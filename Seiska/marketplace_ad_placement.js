/*
Domain: Seiska.fi
Experiment name: Marketplace ad experiment
Version: 1.0
Platform: VWO
Author: Thomas
*/

//Variable for Uutiset section
var uutisetContainer = document.getElementsByClassName('category-container')[0];

//Add marketplace ad box after Uutiset section
uutisetContainer.innerHTML += "<div id='marketplaceAd'></div>";

//Variable for marketplace ad box
var marketplaceAd = document.getElementById('marketplaceAd');

//Brand array
var brands = ["idealista","seiska",'baana','suomi24','katso','treffit.suomi24'];
//Banner image array
var bannerURLS = ["https://s17.postimg.cc/irxg0ja1b/165x250_test_idealista.jpg","https://s17.postimg.cc/6dao08l3z/165x250_test_seiska.jpg",'https://s17.postimg.cc/pv5bg5n6n/165x250_test_baana.jpg','https://s17.postimg.cc/72tgcl67z/165x250_test_s24.jpg','https://s17.postimg.cc/d3r59nl4f/165x250_test_katso.jpg','https://s17.postimg.cc/pidx9zs27/165x250_test_s24t.jpg'];

//Banner constructor
for (i = 0; i < brands.length; i++) {
  marketplaceAd.innerHTML += '<a class="marketplaceBox" href="http://'+ brands[i]+'.fi?utm_source=seiska_marketplace&utm_medium=experiment&utm_campaign='+brands[i]+'"><img alt='+brands[i]+' src='+bannerURLS[i]+'></a>';
}

//Hide overlap of text from articles
for ( k = 0; k < 9; k++ ){
  uutisetContainer.querySelectorAll('article .caption')[k].style = "height: auto;";
}

//Styles
marketplaceAd.style = "border: 5px solid #f2f2f2; margin: 40px auto;";
var marketplaceBox = document.getElementsByClassName('marketplaceBox');
for (j = 0; j < marketplaceBox.length; j++ ) {
    marketplaceBox[j].style = "cursor: pointer; text-align: center; width: 165px; height: 250px; margin: 20px 14px; background: #efefef; display: inline-block;";
}
