/*
Domain: Seiska.fi
Experiment name: Seiska mobile search functionality
Version: 1.0
Platform: Optimizely
Author: Thomas
*/

//Append search box
$('.large-card').prepend('<div id="seiska_search_bar"></div>');
$('#seiska_search_bar').append('<input type="text" id="seiska_search" placeholder="Hae sisältöä">');
$('#seiska_search_bar').append('<input type="submit" id="seiska_search_button" value="Hae">');

//Styles

$('#seiska_search_bar').css({
    'background' : '#ffe300'
});

$('input#seiska_search').css({
  'height' : '50px',
  'padding' : '0px 0 0 15px',
  'margin' : '20px 0px 20px 20px',
  'width' : '70%',
  'border' : '1px solid #c0c0c0',
  'position' : 'relative',
  'top' : '1px',
  'border-radius' :'0'
});

$('input#seiska_search_button').css({
  'height' : '49.5px',
  'border' : '0',
  'background' : '#ec1d24',
  'color' : '#fff',
  'font-weight' : 'bold',
  'text-transform' : 'uppercase',
  'width' : '20%',
  'border-radius' :'0'
});

//Onclick event, search funcationality
$('input#seiska_search_button').click(function(){
  //DEFINE URL's
  var searchQuery = $('input#seiska_search').val();
  var redirectURL = "http://google.fi/search?q="+searchQuery+"&as_sitesearch=seiska.fi";
  window.location.href = redirectURL;
});

//Enter key event
$("input#seiska_search").keyup(function (e) {
  //DEFINE URL's
  var searchQuery = $('input#seiska_search').val();
  var redirectURL = "http://google.fi/search?q="+searchQuery+"&as_sitesearch=seiska.fi";
    if (e.which == 13) {
      window.location.href = redirectURL;
      // ensures the optimizely object is defined globally using
      window['optimizely'] = window['optimizely'] || [];
      // sends a tracking call to Optimizely for the given event name.
      window.optimizely.push(["trackEvent", "enterSearch"]);
    }
});
