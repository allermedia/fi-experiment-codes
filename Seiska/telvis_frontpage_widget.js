/*
Domain: Seiska.fi
Experiment name: Telvis embed banner on front page
Version: 1.0
Platform: Optimizely
Author: Thomas
*/


//Widget
jQuery('<a href="https://telvis.fi"><div id="leffat_tanaan"></div></a>').insertAfter('.listing-title--telkkari');
//Button
jQuery('#leffat_tanaan').append('<a id="leffat_tanaan__header" href="https://telvis.fi" class="btn btn-primary">Leffat tänään</a>');

//YLE
//jQuery('#leffat_tanaan').append('<div class="tvShow"><img src="https://s17.postimg.org/m8oa7ggrz/yle_tv1.png"><b>	Underworld: Rise of the Lycans -</b><span> 21:00</span></div>');
//jQuery('#leffat_tanaan').append('<div class="tvShow"><img src="https://s17.postimg.org/45v7g8sn3/yle_tv2.png"><b>	Wrecked -</b><span> 21:00</span></div>');
//MTV3
//jQuery('#leffat_tanaan').append('<div class="tvShow"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Mtv3_new_logo.svg/1200px-Mtv3_new_logo.svg.png"><b>Terminator Salvation -</b><span> 21:00</span></div>');
//Nelonen
//jQuery('#leffat_tanaan').append('<div class="tvShow"><img src="https://s17.postimg.org/5tyy8sgin/nelonen.jpg"><b>The Dictator -</b><span> 22:00</span></div>');
//Sub
jQuery('#leffat_tanaan').append('<div class="tvShow"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0e/Sub_new_logo.svg/1200px-Sub_new_logo.svg.png"><b>	xXx -</b><span> 21:00</span></div>');
//Jim
//jQuery('#leffat_tanaan').append('<div class="tvShow"><img src="https://www.underconsideration.com/brandnew/archives/jim_tv_logo.png"><b>	Wrecked -</b><span> 21:00</span></div>');
//TV5
jQuery('#leffat_tanaan').append('<div class="tvShow"><img src="http://discoverynetworks.fi/wp-content/uploads/2015/06/K5_logo_White.jpg"><b>Fast Five -</b><span> 21:00</span></div>');
//Kutonen
jQuery('#leffat_tanaan').append('<div class="tvShow"><img src="http://kutonen.fi/sites/default/files/KUTONEN_FINAL_LOGO.png"><b>	Annetaaan Palaa -</b><span> 21:00</span></div>');
//Frii
//jQuery('#leffat_tanaan').append('<div class="tvShow"><img src="https://upload.wikimedia.org/wikipedia/fi/a/ab/Frii_logo.png"><b>	Wrecked -</b><span> 21:00</span></div>');

//Sponsor
jQuery('#leffat_tanaan').append('<div class="sponsor"><span id="provided_by">Palvelun tuottaa</span><a href="https://telvis.fi"><img id="telvis" src="https://www.telvis.fi/static/img/telvis-logo.png"></a></div>');

//Styles
jQuery('#leffat_tanaan').css({
  'margin' : '20px 0 10px -5px',
  'background' : '#000',
  'color' : '#fff',
  'padding' : '1% 1% 1% 0',
  'font-weight' : '400'
});

jQuery('#leffat_tanaan__header').css({
  'padding' : '20px',
  'position' : 'relative',
  'top' : '-12px',
  'margin-bottom' : '-24px'
});

jQuery('#leffat_tanaan .tvShow').css({
  'display' : 'inline',
  'padding' : '0 20px',
  'position' : 'relative',
  'top' : '3px'
});

jQuery('#leffat_tanaan img').css({
  'max-height' : '20px',
  'margin' : '0 5px 0 0'
});

jQuery('.sponsor').css({
  'float' : 'right',
  'padding' : '7.5px 10px 0px 0px'
});

jQuery('span#provided_by').css({
  'font-size' : '.75em',
  'margin' : '0 10px 0 0'
});

jQuery('img#telvis').css({
  'width': '70px'
});

if ( window.innerWidth < 768 ) {

  jQuery('#leffat_tanaan__header').css({
    'padding' : '10px',
    'top' : '-6px',
    'margin-bottom' : '5px',
    'width' : '101%'
  });

  jQuery('#leffat_tanaan .tvShow').css({
    'display' : 'block',
    'padding' : '0 0 20px 20px'

  });

  jQuery('.sponsor').css({
    'padding' : '7.5px 10px 10px 20px',
    'float' : 'initial'
  });

  jQuery('#leffat_tanaan img').css({
    'width' : '40px'
  });

  jQuery('.tvShow span, .tvShow b').css({
    'position' : 'relative',
    'left' : '10px',
    'bottom' : '0px'
  });

}
