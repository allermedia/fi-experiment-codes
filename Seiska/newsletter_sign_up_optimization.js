/*
Domain: Seiska.fi
Experiment name: Newsletter sign up box optimization
Version: 1.0
Platform: VWO
Author: Thomas
*/

//Variation 1 (Teme)

//Change background image
document.getElementsByClassName('newsletter--wrapper')[0].style.background = "url(https://i.postimg.cc/sDTx6F4P/teme.png) no-repeat -15% 10%, url(https://static.soihtu.eu/tilaus/css/seiska/180605-bg.jpg) 100%";
//Font color
document.getElementsByClassName('newsletter--wrapper')[0].style.color = "#000";
//Remove gray opacity layer
document.getElementsByClassName('newsletter--text-wrapper')[0].style.background = "rgba(0,0,0,0)";
//Styles
document.getElementsByClassName('newsletter--subscribe')[0].style.margin = "0 0 0 200px";

//Mobile styles
if ( window.innerWidth < 650 ) {
  document.getElementsByClassName('newsletter--subscribe')[0].style.margin = "0 0 0 0";
  document.getElementsByClassName('newsletter--text-wrapper')[0].style.background = "rgba(0,0,0,.25)";
  document.getElementsByClassName('newsletter--wrapper')[0].style.color = "#FFF";
}

//Variation 2 (Manda)

//Change background image
document.getElementsByClassName('newsletter--wrapper')[0].style.background = "url(https://i.postimg.cc/vHYDt8fH/manda.png) no-repeat 100% -10%, url(https://static.soihtu.eu/tilaus/css/seiska/180605-bg.jpg) 100%";
//Font color
document.getElementsByClassName('newsletter--wrapper')[0].style.color = "#000";
//Remove gray opacity layer
document.getElementsByClassName('newsletter--text-wrapper')[0].style.background = "rgba(0,0,0,0)";
//Styles
document.getElementsByClassName('newsletter--subscribe')[0].style.margin = "0 0 0 -200px";

//Mobile styles
if ( window.innerWidth < 650 ) {
  document.getElementsByClassName('newsletter--subscribe')[0].style.margin = "0 0 0 0";
  document.getElementsByClassName('newsletter--text-wrapper')[0].style.background = "rgba(0,0,0,.25)";
  document.getElementsByClassName('newsletter--wrapper')[0].style.color = "#FFF";
}

//Variation 3 (Casino background)
//Change background
document.getElementsByClassName('newsletter--wrapper')[0].style.background = "url('https://i.postimg.cc/sfKsBr6C/bg_casino.png')";
document.getElementsByClassName('newsletter--wrapper')[0].style.borderImage = "url('https://i.postimg.cc/65JZjCVh/border_kehys.png')";
document.getElementsByClassName('newsletter--wrapper')[0].style.border = "12.5px dotted #ffb133";

//Variation 4 (Trump)
//Change background image
document.getElementsByClassName('newsletter--wrapper')[0].style.background = "url(https://i.postimg.cc/8CjcgqYL/trump_fake.png) no-repeat -55% -40%, url(https://i.postimg.cc/sfKsBr6C/bg_casino.png) 100%";
//Font color
document.getElementsByClassName('newsletter--wrapper')[0].style.color = "#000";
//Remove gray opacity layer
document.getElementsByClassName('newsletter--text-wrapper')[0].style.background = "rgba(0,0,0,0)";
//Styles
document.getElementsByClassName('newsletter--subscribe')[0].style.margin = "0 0 0 200px";

//Mobile styles
if ( window.innerWidth < 650 ) {
  document.getElementsByClassName('newsletter--subscribe')[0].style.margin = "0 0 0 0";
  document.getElementsByClassName('newsletter--text-wrapper')[0].style.background = "rgba(0,0,0,.25)";
  document.getElementsByClassName('newsletter--wrapper')[0].style.color = "#FFF";
}
    
