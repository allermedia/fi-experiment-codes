/*
Domain: Seiska.fi
Experiment name: Soihtu product optimization experiment
Version: 1.0
Platform: Optimizely
Author: Thomas
*/

//Add CTA text to products
$('#selection-wrap h3').html('VALITSE TUOTE');

//Remove paragraph text
$('#selection-wrap p:eq(1)').hide();

//Add Valitse (select) buttons
$('#sel1, #sel2, #sel3').append('<br><div id="submit"><input type="submit" class="selectProduct" style="text-align: center;" value="Valitse"></div>');

//Styles
$('p.magdesc').css({
  'height' : '40px',
  'display' : 'block',
  'font-size' : '1.1em',
  'margin' : '10px 0 0 0'
});

$('#sel1, #sel2, #sel3').css({
  'cursor': 'pointer',
  'background' : '#fff',
  'padding': '25px',
  'margin' : '0 7px',
  'border' : '1px solid #fff',
  'border-radius' : '5px'
});

$('#selitem-wrap #submit input').css({
  'width' : '80%'
});

$('.selitem').css({
  'width' : '25%'
});

$('#selitem-wrap').css('max-width','100%');

//Hover and click events
$('#selection img').hover(function(){
    $(this).css('-webkit-filter' , 'brightness(100%)');
    $(this).css('filter' , 'brightness(100%)');
  }
);

$('#selection img').click(function(){
    $(this).css('-webkit-filter' , 'brightness(100%)');
    $(this).css('filter' , 'brightness(100%)');
  }
);

$('#sel1, #sel2, #sel3').hover(function(){
    $(this).css('border','1px solid #eee');
  }, function(){
    $(this).css('border','1px solid #fff');
  }
);

//Mobile styles (under 1000px)
if (window.innerWidth < 1000 ) {
  $('#sel1, #sel2, #sel3').css({
    'padding': '5px'
    });
  $('.selitem p').css('height','70px');
}
