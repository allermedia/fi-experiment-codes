/*
Domain: Seiska.fi
Experiment name: Soihtu order form optimization experiment
Version: 1.0
Platform: Optimizely
Author: Thomas
*/

//Remove animated snow
$('div:contains(•)').remove();

//CTA text to products
$('#selection-wrap h3').html('VALITSE TUOTE');

//Remove paragraph text
$('#selection-wrap p:eq(1)').hide();

//Add valitse buttons
$('#sel1, #sel2, #sel3').append('<br><div id="selectProduct"><input type="submit" class="selectProduct" style="text-align: center;" value="Valitse"></div>');

$('#sel1, #sel2, #sel3').css({
  'cursor': 'pointer',
  'background' : '#fff',
  'padding': '25px',
  'margin' : '0 7px',
  'border' : '1px solid #fff',
  'border-radius' : '5px'
});

$('.selectProduct').css({
  'border' : 'none',
  'background' : 'green',
  'color' : '#fff',
  'padding' : '10px'
});

$('#selection').css('margin','-1%');

$('#selitem-wrap #submit input').css({
  'width' : '100%'
});

$('.selitem').css({
  'width' : '25%'
});

$('p.magdesc').css({
  'height' : '40px',
  'display' : 'block',
  'font-size' : '1.1em',
  'margin' : '10px 0 0 0'
});

//Hover and click functions
$('#selection img').hover(function(){
    $(this).css('-webkit-filter' , 'brightness(100%)');
    $(this).css('filter' , 'brightness(100%)');
  }
);

$('#selection img').click(function(){
    $(this).css('-webkit-filter' , 'brightness(100%)');
    $(this).css('filter' , 'brightness(100%)');
  }
);

$('#sel1, #sel2, #sel3').hover(function(){
    $(this).css('border','1px solid #eee');
  }, function(){
    $(this).css('border','1px solid #fff');
  }
);

//Form styles
$('.input input').css({
  'background' : '#fff',
  'height' : '45px',
  'padding' : '0 0 0 10px',
  'font-size' : '1em'
});

$('.select select').css({
  'color' : '#757575',
  'font-size' : '.95em',
});

$('.select select').css({
  'height' : '45px',
  'background' : '#fff',
  'width' : '100%'
});

$('p.giftorder').css({
  'cursor' : 'pointer',
  'margin' : '20px 0 20px 0',
  'display' : 'block',
  'max-width' : '110px'
});

$('#submit input').css('border','none');

$('input#giftcheck').css({
    'zoom' : '1.5',
    'position' : 'relative',
    'top' : '1px',
    'left' : '5px'
});

$('#buttons').css({
  'float' : 'none',
  'text-align' : 'left'
});

//Edit big headline
$('.campaign_sweepstake_text p').html('<p>Kaikkien tilaajien kesken järjestetään arvonta - <br/>voit voittaa <b>35 000 €</b> puhtaana käteen!</p>');

//Styles
$('.campaign_sweepstake_text p').css({
  'font-size' : '1.6em',
  'text-align' : 'center'
});

$('.campaign_sweepstake_text b').css({
  'font-weight' : 'bold'
});

$('.campaign_sweepstake_text').css({
  'float' : 'none',
  'margin' : '0 0 20px 0'
});

$('.campaigninfo').css({
  'float' : 'none',
  'font-size' : '1em',
  'margin' : '0px 0px 20px 0px',
  'width' : '50%',
  'display' : 'block',
  'position' : 'relative',
  'top' : '7.5px'
});

$('.campaigninfo_sms, .campaigninfo_email').css({
  'float' : 'none',
  'font-size' : '1em'
});

//Select option texts
$('select#yearofbirth option:eq(0)').text('Syntymävuosi');
$('select#yearofbirth option').css('background','#fff');

//Hide form input label
$('.label').remove();

//Click function to gift options
$('p.giftorder').click(function(){
    $('#giftfields').toggle();
});

$('#giftfields').css('margin','20px 0px');

//Move submit buttons to form area
$('#buttons').appendTo('#fields');

//Change copy in offer link + move element + styles
$('#sixmonths p a').text('Oletko tilannut Seiskaa viimeisen 6 kk:n aikana?');
$('#sixmonths').appendTo('#buttons');
$('#sixmonths p a').css({
  'color' : '#369',
  'font-weight' : '100'
});

$('#sixmonths').css('margin','20px 0 0 0');

//Move product selection to order form
$('#addtosweepstake').prependTo('#fields');

//Move input fields
$('#email_div').insertBefore('#address_div');
$('#phone_div').insertBefore('#address_div');
$('#city_div').insertBefore('#postalcode_div');

$('.field').css({
  'width' : '22%',
  'float' : 'none',
  'display' : 'inline-block'
});

$('#address_div').css('width' , '46.75%', '!important');

$('input#city, input#postalcode').css({
  'position' : 'relative',
  'left' : '-3px'
});

//Product selection styles
$('#addtosweepstake').css({
  'font-size' : '1.5em'
});

//Payment headline text
$('p.payment_method').text('Maksutapa');

//Add new CTA buttons
$('<label><input type="radio" id="verkkopankki">Verkkopankki</label></input><label><input type="radio" id="luottokortti">Luottokortti</label><label><input type="radio" id="lasku">Lasku</label><div id="submitCTA">Tilaa</div>').insertAfter('.payment_method');

//Submit button styles
$('#submitCTA').css({
  'background': '#379e1b',
  'width' : '500px',
  'padding' : '20px 0',
  'text-align' : 'center',
  'color' : '#fff',
  'cursor' : 'pointer',
  'margin': '10px 0px 30px',
  'font-size' : '1.25em'
});

$('#buttons input').css({
  'zoom' : '1.75',
  'position' : 'relative',
  'top' : '3px',
  'margin' : '0 5px 0 0'
});

$('#buttons label').css({
  'margin' : '0 20px 10px 0',
  'display' : 'inline-block',
  'cursor' : 'pointer',
  'font-size' : '1.2em'
});

//Hide original payment CTA buttons
$('.highlight').hide();

//Click functions for product selections
$('#submitCTA').click(function(){
    if ($('input#verkkopankki').is(':checked')) {
		    $('#verkkopankkibtn').click();
    }
    if ($('input#luottokortti').is(':checked')) {
        $('#luottokorttibtn').click();
    }
    if ($('input#lasku').is(':checked')) {
        $('#laskubtn').click();
    }
});

$('.selitem').click(function(){
  $('#addtosweepstake').css('margin','0px 0px 40px');
  $('#addtosweepstake em').css('color' , '#000','important');
  //Add checkbox to selected product
  $('#addtosweepstake').prepend('<img src="https://s14.postimg.org/5m5dtzemp/arrow_check.png">');
    //Styles for seleted product text
  $('#addtosweepstake img').css({
    'display' : 'inline',
    'width' : '50px',
    'position' : 'relative',
    'top' : '15px',
    'margin' : '0 10px 0 0'
  });
});

//Font weight on form footer links
$('#sixmonths a, #rules a').css('font-weight' , '100');

//Campaign disclaimer / info + info
$('#rules div').css('display','block');
$('#rules div a').css('color','#369');
$('#rules').css('font-size','1em');

//Move order info text
$('.order_info').insertAfter('#submitCTA');
$('.order_info a').css({
  'color' : '#369',
  'font-weight': '100'
});
$('.order_info').css('width', '53%');

//Order info headline
$('.order_information, .payment_method').css('margin' , '0 0 20px 0');

//Disclaimer after CTA area + styles
$('.continous_offertext').appendTo('.order_info');
$('.continous_offertext').css({
  'font-size' : '1em',
  'float' : 'none'
});

//Remove label
$('.label label').remove();

//Mobile styles
if ( window.innerWidth < 840 ) {
  $('#submitCTA').css({
    'width' : '100%'
  });
  $('p.order_info, span.campaigninfo').css({
    'width' : '90%'
  });
  $('.field').css({
    'width' : '47%'
  });
  $('.field input').css({
    'left' : '0'
  });
  $('.campaign_sweepstake_text').css('margin','0');
  $('.campaign_sweepstake_text p').css('font-size','1.2em');
  $('p.giftorder').css('max-with','140px');
  $('input#phone').css({
  	'position' : 'relative',
  	'left' : '5px'
  });
  $('select#yearofbirth').css('padding-left','10px');
}

if (window.innerWidth < 1000 ) {
  $('#sel1, #sel2, #sel3').css({
    'padding': '5px'
    });
  $('.giftorder').css('max-width','91%');

  $('.selitem p').css('height','70px');
}

//HEADER AWAY
$('body').css('background','none');
$('#offers, header').remove();
